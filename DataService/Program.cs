﻿using System;
using System.Diagnostics;

namespace DataService
{
    class Program
    {
        static void Main(string[] args)
        {
            var psi = new ProcessStartInfo();
            psi.FileName = @"C:\Users\DELL\AppData\Local\Programs\Python\Python37\python.exe";
            var script = @"D:\CapstoneProject\Detect-Disease-Vegetable-System\detectDisease.py";
            psi.Arguments = script;
            //Console.WriteLine(psi.Arguments);
            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;
            var error = "";
            var result = "";
            using (var process = Process.Start(psi))
            {
                error = process.StandardError.ReadToEnd();
                result = process.StandardOutput.ReadToEnd();
            }
            Console.WriteLine(error);
            Console.WriteLine(result);
        }
    }
}
