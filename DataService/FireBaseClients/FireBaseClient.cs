﻿using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DataService.FireBaseClients
{
    public static class FirebaseClient
    {
        static FirebaseClient()
        {
            var currentDir = Environment.CurrentDirectory;//chay len coi thu no tro toi dau

            FirebaseApp.Create(new AppOptions()
            {
                //Credential = GoogleCredential.FromFile(Path.Combine(currentDir, "App/firebase-config.json"))
                Credential = GoogleCredential.FromFile("D:\\Capstone-Web-API\\DataService\\App\\firebase-config.json")
            }) ;
        }

        public static bool Send(string token, Notification noti, Dictionary<string, string> data)
        {
            var message = new Message()
            {
                Notification = noti,
                Data = data,
                Token = token,
            };
            try
            {
                var resp = FirebaseMessaging.DefaultInstance.SendAsync(message).Result;
                return true;
            }
            catch (Exception)
            {

                return false;
            }
            
        }

    }
}
