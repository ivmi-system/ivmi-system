﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DataService.Models
{
    public partial class CapstoneDbContext : DbContext
    {
        public CapstoneDbContext()
        {
        }

        public CapstoneDbContext(DbContextOptions<CapstoneDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Contract> Contract { get; set; }
        public virtual DbSet<Disease> Disease { get; set; }
        public virtual DbSet<ImageDisease> ImageDisease { get; set; }
        public virtual DbSet<Notification> Notification { get; set; }
        public virtual DbSet<NotificationPotVegetableDisease> NotificationPotVegetableDisease { get; set; }
        public virtual DbSet<Plot> Plot { get; set; }
        public virtual DbSet<Pot> Pot { get; set; }
        public virtual DbSet<PotVegetable> PotVegetable { get; set; }
        public virtual DbSet<PotVegetableDisease> PotVegetableDisease { get; set; }
        public virtual DbSet<Request> Request { get; set; }
        public virtual DbSet<Schedule> Schedule { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Vegetable> Vegetable { get; set; }
        public virtual DbSet<VegetableDisease> VegetableDisease { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=13.250.59.224;Database=Capstone;user=khoi_capstone;pwd=kho!123;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contract>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DateCreate).HasColumnType("datetime");

                entity.Property(e => e.DateStart).HasColumnType("datetime");

                entity.Property(e => e.Duration).HasColumnType("datetime");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.Status).HasDefaultValueSql("((0))");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.Contract)
                    .HasForeignKey(d => d.Username)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Contract_User");
            });

            modelBuilder.Entity<Disease>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Description).HasColumnType("text");

                entity.Property(e => e.LabelId).HasColumnName("LabelID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);
            });

            modelBuilder.Entity<ImageDisease>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FeedbackContent).HasMaxLength(255);

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.Path).HasMaxLength(255);

                entity.Property(e => e.VegetableId).HasColumnName("VegetableID");
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DateTime).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(150);

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsSeen).HasDefaultValueSql("((0))");

                entity.Property(e => e.PlotId).HasColumnName("PlotID");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Plot)
                    .WithMany(p => p.Notification)
                    .HasForeignKey(d => d.PlotId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Notification_Plot");

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.Notification)
                    .HasForeignKey(d => d.Username)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Notification_User");
            });

            modelBuilder.Entity<NotificationPotVegetableDisease>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.NotificationId).HasColumnName("NotificationID");

                entity.Property(e => e.PotVegetableDiseaseId).HasColumnName("PotVegetableDiseaseID");

                entity.HasOne(d => d.Notification)
                    .WithMany(p => p.NotificationPotVegetableDisease)
                    .HasForeignKey(d => d.NotificationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NotificationPotVegetableDisease_Notification");

                entity.HasOne(d => d.PotVegetableDisease)
                    .WithMany(p => p.NotificationPotVegetableDisease)
                    .HasForeignKey(d => d.PotVegetableDiseaseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_NotificationPotVegetableDisease_PotVegetableDisease");
            });

            modelBuilder.Entity<Plot>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.Line).HasMaxLength(100);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.Plot)
                    .HasForeignKey(d => d.Username)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Plot_User");
            });

            modelBuilder.Entity<Pot>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Coordinates)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.PlotId).HasColumnName("PlotID");

                entity.Property(e => e.Status).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.Plot)
                    .WithMany(p => p.Pot)
                    .HasForeignKey(d => d.PlotId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Pot_Plot");
            });

            modelBuilder.Entity<PotVegetable>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DateHarvest).HasColumnType("datetime");

                entity.Property(e => e.DatePlant).HasColumnType("datetime");

                entity.Property(e => e.HasWorm).HasDefaultValueSql("((0))");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.PotId).HasColumnName("PotID");

                entity.Property(e => e.UrlImage)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.VegetableId).HasColumnName("VegetableID");
            });

            modelBuilder.Entity<PotVegetableDisease>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.DateBegin).HasColumnType("datetime");

                entity.Property(e => e.Description).HasColumnType("text");

                entity.Property(e => e.DiseaseId).HasColumnName("DiseaseID");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.PotVegetableId).HasColumnName("PotVegetableID");

                entity.HasOne(d => d.Disease)
                    .WithMany(p => p.PotVegetableDisease)
                    .HasForeignKey(d => d.DiseaseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PotVegetableDisease_Disease");

                entity.HasOne(d => d.PotVegetable)
                    .WithMany(p => p.PotVegetableDisease)
                    .HasForeignKey(d => d.PotVegetableId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PotVegetableDisease_PotVegetable");
            });

            modelBuilder.Entity<Request>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Schedule>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.Schedule)
                    .HasForeignKey(d => d.Username)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Schedule_User");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.Username);

                entity.Property(e => e.Username)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Address).HasMaxLength(150);

                entity.Property(e => e.CameraIp)
                    .HasColumnName("CameraIP")
                    .HasMaxLength(150);

                entity.Property(e => e.DeviceToken)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.Name).HasMaxLength(150);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Vegetable>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Description).HasColumnType("text");

                entity.Property(e => e.IsDelete).HasDefaultValueSql("((0))");

                entity.Property(e => e.LabelId).HasColumnName("LabelID");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Thumbnail).HasMaxLength(255);
            });

            modelBuilder.Entity<VegetableDisease>(entity =>
            {
                entity.HasKey(e => new { e.VegetableId, e.DiseaseId });

                entity.Property(e => e.VegetableId).HasColumnName("VegetableID");

                entity.Property(e => e.DiseaseId).HasColumnName("DiseaseID");

                entity.HasOne(d => d.Disease)
                    .WithMany(p => p.VegetableDisease)
                    .HasForeignKey(d => d.DiseaseId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VegetableDisease_Disease");

                entity.HasOne(d => d.Vegetable)
                    .WithMany(p => p.VegetableDisease)
                    .HasForeignKey(d => d.VegetableId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VegetableDisease_Vegetable");
            });
        }
    }
}
