﻿using System;
using System.Collections.Generic;

namespace DataService.Models
{
    public partial class Temp
    {
        public int Id { get; set; }
        public string VegetableName { get; set; }
        public int? Pot { get; set; }
        public int? SauAn { get; set; }
        public int? VangLa { get; set; }
        public int? ConSau { get; set; }
        public int? LaNon { get; set; }
        public string UrlImage { get; set; }
        public int? LaHeo { get; set; }
        public bool? IsDelete { get; set; }
    }
}
