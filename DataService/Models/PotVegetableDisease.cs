﻿using System;
using System.Collections.Generic;

namespace DataService.Models
{
    public partial class PotVegetableDisease
    {
        public PotVegetableDisease()
        {
            NotificationPotVegetableDisease = new HashSet<NotificationPotVegetableDisease>();
        }

        public int Id { get; set; }
        public int PotVegetableId { get; set; }
        public int DiseaseId { get; set; }
        public DateTime? DateBegin { get; set; }
        public int? Status { get; set; }
        public string Description { get; set; }
        public bool? IsDelete { get; set; }

        public Disease Disease { get; set; }
        public PotVegetable PotVegetable { get; set; }
        public ICollection<NotificationPotVegetableDisease> NotificationPotVegetableDisease { get; set; }
    }
}
