﻿using System;
using System.Collections.Generic;

namespace DataService.Models
{
    public partial class Request
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public long CardNumber { get; set; }
        public int Budget { get; set; }
        public int Status { get; set; }
        public bool IsDelete { get; set; }
    }
}
