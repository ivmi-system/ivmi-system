﻿using System;
using System.Collections.Generic;

namespace DataService.Models
{
    public partial class User
    {
        public User()
        {
            Contract = new HashSet<Contract>();
            Notification = new HashSet<Notification>();
            Plot = new HashSet<Plot>();
            Schedule = new HashSet<Schedule>();
        }

        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string DeviceToken { get; set; }
        public string Phone { get; set; }
        public int Role { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string CameraIp { get; set; }
        public bool? IsDelete { get; set; }

        public ICollection<Contract> Contract { get; set; }
        public ICollection<Notification> Notification { get; set; }
        public ICollection<Plot> Plot { get; set; }
        public ICollection<Schedule> Schedule { get; set; }
    }
}
