﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.Models.ModelCreate
{
    public class VegetableModel
    {
        public int Pot { get; set; }
        public string UrlImage { get; set; }
        public int CayCon { get; set; }
        public int ConSau { get; set; }
        public int Blank { get; set; }
        public List<int> listDisease { get; set; }
        public string Name { get; set; }

    }
}
