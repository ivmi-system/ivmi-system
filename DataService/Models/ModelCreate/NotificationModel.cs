﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.Models.ModelCreate
{
    public class NotificationModel
    {
        public string username { get; set; }
        public int plotId { get; set; }
        List<int> diseaseIds { get; set; }
        List<string> messages { get; set; }
    }
}
