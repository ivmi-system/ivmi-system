﻿using System;
using System.Collections.Generic;

namespace DataService.Models
{
    public partial class Plot
    {
        public Plot()
        {
            Notification = new HashSet<Notification>();
            Pot = new HashSet<Pot>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string Line { get; set; }
        public int? Size { get; set; }
        public bool? IsDelete { get; set; }

        public User UsernameNavigation { get; set; }
        public ICollection<Notification> Notification { get; set; }
        public ICollection<Pot> Pot { get; set; }
    }
}
