﻿using System;
using System.Collections.Generic;

namespace DataService.Models
{
    public partial class Schedule
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public bool? IsDelete { get; set; }
        public TimeSpan? Time { get; set; }

        public User UsernameNavigation { get; set; }
    }
}
