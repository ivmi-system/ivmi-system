﻿using System;
using System.Collections.Generic;

namespace DataService.Models
{
    public partial class PotVegetable
    {
        public PotVegetable()
        {
            PotVegetableDisease = new HashSet<PotVegetableDisease>();
        }

        public int Id { get; set; }
        public int PotId { get; set; }
        public int VegetableId { get; set; }
        public DateTime? DatePlant { get; set; }
        public DateTime? DateHarvest { get; set; }
        public int? Status { get; set; }
        public string UrlImage { get; set; }
        public bool? HasWorm { get; set; }
        public bool? IsDelete { get; set; }

        public ICollection<PotVegetableDisease> PotVegetableDisease { get; set; }
    }
}
