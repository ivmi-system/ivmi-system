﻿using System;
using System.Collections.Generic;

namespace DataService.Models
{
    public partial class VegetableDisease
    {
        public int VegetableId { get; set; }
        public int DiseaseId { get; set; }
        public bool IsDelete { get; set; }

        public Disease Disease { get; set; }
        public Vegetable Vegetable { get; set; }
    }
}
