﻿using System;
using System.Collections.Generic;

namespace DataService.Models
{
    public partial class Disease
    {
        public Disease()
        {
            PotVegetableDisease = new HashSet<PotVegetableDisease>();
            VegetableDisease = new HashSet<VegetableDisease>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? LabelId { get; set; }
        public string Description { get; set; }
        public bool IsDelete { get; set; }

        public ICollection<PotVegetableDisease> PotVegetableDisease { get; set; }
        public ICollection<VegetableDisease> VegetableDisease { get; set; }
    }
}
