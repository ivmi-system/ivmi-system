﻿using System;
using System.Collections.Generic;

namespace DataService.Models
{
    public partial class ImageDisease
    {
        public int Id { get; set; }
        public int VegetableId { get; set; }
        public string Path { get; set; }
        public bool? IsDelete { get; set; }
        public string FeedbackContent { get; set; }
    }
}
