﻿using System;
using System.Collections.Generic;

namespace DataService.Models
{
    public partial class Pot
    {
        public int Id { get; set; }
        public int PlotId { get; set; }
        public int? Status { get; set; }
        public bool? IsDelete { get; set; }
        public string Coordinates { get; set; }
        public int? Position { get; set; }

        public Plot Plot { get; set; }
    }
}
