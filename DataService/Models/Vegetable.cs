﻿using System;
using System.Collections.Generic;

namespace DataService.Models
{
    public partial class Vegetable
    {
        public Vegetable()
        {
            VegetableDisease = new HashSet<VegetableDisease>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int LabelId { get; set; }
        public int HarvestFrom { get; set; }
        public int HarvestTo { get; set; }
        public string Description { get; set; }
        public string Thumbnail { get; set; }
        public bool? IsDelete { get; set; }

        public ICollection<VegetableDisease> VegetableDisease { get; set; }
    }
}
