﻿using System;
using System.Collections.Generic;

namespace DataService.Models
{
    public partial class Notification
    {
        public Notification()
        {
            NotificationPotVegetableDisease = new HashSet<NotificationPotVegetableDisease>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public int PlotId { get; set; }
        public string Description { get; set; }
        public DateTime? DateTime { get; set; }
        public bool? IsSeen { get; set; }
        public bool? IsDelete { get; set; }

        public Plot Plot { get; set; }
        public User UsernameNavigation { get; set; }
        public ICollection<NotificationPotVegetableDisease> NotificationPotVegetableDisease { get; set; }
    }
}
