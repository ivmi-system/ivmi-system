﻿using System;
using System.Collections.Generic;

namespace DataService.Models
{
    public partial class NotificationPotVegetableDisease
    {
        public int Id { get; set; }
        public int NotificationId { get; set; }
        public int PotVegetableDiseaseId { get; set; }
        public bool? IsDelete { get; set; }

        public Notification Notification { get; set; }
        public PotVegetableDisease PotVegetableDisease { get; set; }
    }
}
