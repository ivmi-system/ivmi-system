﻿using System;
using System.Collections.Generic;

namespace DataService.Models
{
    public partial class Contract
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? Duration { get; set; }
        public double? Fee { get; set; }
        public int? Status { get; set; }
        public bool? IsDelete { get; set; }

        public User UsernameNavigation { get; set; }
    }
}
