﻿using DataService.DesignPatern.Handler;
using DataService.DesignPatern.Security;
using DataService.DesignPatern.UnitOfWorks;
using DataService.Models;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.Services
{
    public interface IHandlerService
    {
        void StartALlSchedules();
    }
    public class HandlerService : BaseUnitOfWork<UnitOfWork>, IHandlerService
    {
        private AppSettings _appSettings;
        public VegetableService _service;
        public HandlerService(UnitOfWork uow, IOptions<AppSettings> appSettings,VegetableService service) : base(uow)
        {
            _appSettings = appSettings.Value;
            _service = service;
        }

        public void StartALlSchedules()
        {
            IEnumerable<Schedule> schedules = _uow.Schedule.GetSchedules();

            if (schedules != null && schedules.Any())
            {
                foreach (Schedule schedule in schedules)
                {
                    var jobName = EncryptorService.MD5Hash(schedule.Username + "_" + schedule.Id);

                    new HandlerJob
                    {
                        Time = schedule.Time,
                        JobName = jobName,
                        Username = schedule.Username,
                        _service = this._service,
                    }.StartJob();
                }
            }
        }
    }
}
