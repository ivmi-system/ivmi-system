﻿using DataService.DesignPatern.Enum;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Security;
using DataService.DesignPatern.ServiceModels;
using DataService.DesignPatern.UnitOfWorks;
using DataService.Models;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Services
{
    public interface IPlotService
    {
        PlotServiceModel GetUserPlotByID(string username, int id);
        PlotServiceModel GetPlotByID(int id);
        PlotServiceModel UpdatePlot(int id, UpdatePlotRequesrModel model);
        bool DeletePlot(int id, bool mode);
        PlotServiceModel CreateNewPlot(CreatePlotRequestModel model);
        IEnumerable<Notification> GetPlotNotifications(int id, int size, int page);
        IEnumerable<Pot> GetPotsByPlotID(int plotID);

    }
    public class PlotService : BaseUnitOfWork<UnitOfWork>, IPlotService
    {
        private readonly AppSettings _appSettings;
       
        public PlotService(UnitOfWork uow, IOptions<AppSettings> appSettings) : base(uow)
        {
            _appSettings = appSettings.Value;
          
        }

        #region Get user's plot by ID
        public PlotServiceModel GetUserPlotByID(string username, int id)
        {
            var plot = _uow.Plot.GetUserPlotByID(username, id);

            if (plot != null)
            {
                PlotServiceModel result = new PlotServiceModel
                {
                    ID = plot.Id,
                    Username = plot.Username,
                    Line = plot.Line,
                    Size = plot.Size,
                    IsDelete = plot.IsDelete
                };

                return result;
            }

            return null;
        }
        #endregion

        #region Get plot by ID
        public PlotServiceModel GetPlotByID(int id)
        {
            var plot = _uow.Plot.GetById(id);

            if (plot != null)
            {
                return new PlotServiceModel
                {
                    ID = plot.Id,
                    Username = plot.Username,
                    Line = plot.Line,
                    IsDelete = plot.IsDelete
                };
            }

            return null;
        }
        #endregion

        #region Update plot
        public PlotServiceModel UpdatePlot(int id, UpdatePlotRequesrModel model)
        {
            var plot = _uow.Plot.GetById(id);

            if (plot != null)
            {
                plot.Line = model.Line;
                plot.Size = model.Size;
                plot.IsDelete = model.IsDelete;

                _uow.Commit();

                return new PlotServiceModel
                {
                    ID = plot.Id,
                    Username = plot.Username,
                    Line = plot.Line,
                    Size = plot.Size,
                    IsDelete = plot.IsDelete
                };
            }

            return null;
        }
        #endregion

        #region Delete plot
        public bool DeletePlot(int id, bool mode)
        {
            var plot = _uow.Plot.GetById(id);

            if (plot != null)
            {
                plot.IsDelete = mode;

                _uow.Commit();

                return true;
            }

            return false;
        }
        #endregion

        #region Create new Plot
        public PlotServiceModel CreateNewPlot(CreatePlotRequestModel model)
        {
            var userUnexist = _uow.User.CheckUsername(model.Username);

            if (!userUnexist)
            {
                var plot = new Plot
                {
                    Username = model.Username,
                    Line = model.Line,
                    Size = model.Size,
                    IsDelete = model.IsDelete
                };

                _uow.Plot.Create(plot);
                _uow.Commit();

                var line = Int32.Parse(model.Line);
                var size = model.Size;

                for (int i = 1; i <= model.Size; i++)
                {
                    // var pos = ((line * size) - ((line * size) / line)) + i;
                    var pos = ((line - 1) * size) + i;
                    CreateNewPot(plot.Id, pos);
                }
                _uow.Commit();

                return new PlotServiceModel
                {
                    ID = plot.Id,
                    Username = plot.Username,
                    Line = plot.Line,
                    Size = plot.Size,
                    IsDelete = plot.IsDelete
                };
            }

            return null;
        }
        #endregion

        #region Create default pot for each plot
        private void CreateNewPot(int plotID, int pos)
        {
            Pot pot = new Pot
            {
                PlotId = plotID,
                Position = pos,
                Status = PotStatus.BLANK,
                IsDelete = false
            };

            _uow.Pot.Create(pot);
        }
        #endregion

        #region Get plot's pot
        public IEnumerable<Pot> GetPlotsPot(int id)
        {
            IEnumerable<Pot> pot = _uow.Pot.GetPotsByPlotID(id);

            if (pot != null && pot.Any())
            {
                return pot;
            }

            return null;
        }
        #endregion

        #region Get plot's notification
        public IEnumerable<Notification> GetPlotNotifications(int id, int size, int page)
        {
            var notification = _uow.Notification.GetPlotNotifications(id, size, page);

            if (notification != null && notification.Any())
            {
                return notification;
            }

            return null;
        }
        #endregion

        #region Get pots by plot ID

        public IEnumerable<Pot> GetPotsByPlotID(int plotID)
        {
            var pots = _uow.Pot.GetPotsByPlotID(plotID);
            if (pots != null && pots.Any())
            {
                return pots;
            }
            return null;
        }

        #endregion
 
    }
}