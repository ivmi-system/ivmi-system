﻿using DataService.DesignPatern.Enum;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.ServiceModels;
using DataService.DesignPatern.UnitOfWorks;
using DataService.Models;
using DataService.Models.ModelCreate;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DataService.DesignPatern.Services
{
    public interface IVegetableService
    {
        //bool ProcessVegetable();
        //IEnumerable<VegetableServiceModel> GetVegetableInfo();
        bool DetectAndSendNoti(string username);
        VegetableServiceModel CreateNewVegetable(CreateVegetableRequestModel model);
        VegetableServiceModel GetVegetableByID(int id);
        VegetableServiceModel UpdateVegetable(int id, UpdateVegetableRequestModel model);
        bool DeleteVegetable(int id, bool mode);
        IEnumerable<Vegetable> GetListVegetables(int size = 10, int page = 1);
        bool ResolveDisease(int potId);
        bool ConfirmYoungVegetable(int potId, int day);

    }
    public class VegetableService : BaseUnitOfWork<UnitOfWork>, IVegetableService
    {
        private readonly CallScriptPythonService _service;
        private readonly NotificationFireBaseService _serviceNoti;



        public VegetableService(UnitOfWork uow, CallScriptPythonService service, NotificationFireBaseService serviceNoti) : base(uow)
        {
            _service = service;
            _serviceNoti = serviceNoti;
        }

        #region Get list vegetable
        public IEnumerable<Vegetable> GetListVegetables(int size = 10, int page = 1)
        {
            var vegetables = _uow.Vegetable.GetListVegetables(size, page);

            if (vegetables != null && vegetables.Any())
            {
                return vegetables;
            }

            return null;
        }
        #endregion

        #region Create new vegetable
        public VegetableServiceModel CreateNewVegetable(CreateVegetableRequestModel model)
        {
            var vegetableLB = _uow.Vegetable.GetVegetableByLableId(model.LabelId);

            if (vegetableLB == null)
            {
                var vegetable = new Vegetable
                {
                    Name = model.Name,
                    LabelId = model.LabelId,
                    HarvestFrom = model.HarvestFrom,
                    HarvestTo = model.HarvestTo,
                    Description = model.Description,
                    Thumbnail = model.Thumbnail,
                    IsDelete = model.IsDelete
                };

                _uow.Vegetable.Create(vegetable);
                _uow.Commit();

                return new VegetableServiceModel
                {
                    ID = vegetable.Id,
                    Name = vegetable.Name,
                    LabelId = vegetable.LabelId,
                    HarvestFrom = vegetable.HarvestFrom,
                    HarvestTo = vegetable.HarvestTo,
                    Description = vegetable.Description,
                    Thumbnail = vegetable.Thumbnail,
                    IsDelete = vegetable.IsDelete
                };
            }

            return null;
        }
        #endregion

        #region Get vegetable by id
        public VegetableServiceModel GetVegetableByID(int id)
        {
            var vegetable = _uow.Vegetable.GetById(id);

            if (vegetable != null)
            {
                return new VegetableServiceModel
                {
                    ID = vegetable.Id,
                    Name = vegetable.Name,
                    LabelId = vegetable.LabelId,
                    HarvestFrom = vegetable.HarvestFrom,
                    HarvestTo = vegetable.HarvestTo,
                    Description = vegetable.Description,
                    Thumbnail = vegetable.Thumbnail,
                    IsDelete = vegetable.IsDelete
                };
            }

            return null;
        }
        #endregion

        #region Update vegetable
        public VegetableServiceModel UpdateVegetable(int id, UpdateVegetableRequestModel model)
        {
            var vegetable = _uow.Vegetable.GetById(id);

            if (vegetable != null)
            {
                vegetable.Name = model.Name;
                // vegetable.LableId = model.LableId;
                vegetable.HarvestFrom = model.HarvestFrom;
                vegetable.HarvestTo = model.HarvestTo;
                vegetable.Description = model.Description;
                vegetable.Thumbnail = model.Thumbnail;
                vegetable.IsDelete = model.IsDelete;

                _uow.Commit();

                return new VegetableServiceModel
                {
                    ID = vegetable.Id,
                    Name = vegetable.Name,
                    LabelId = vegetable.LabelId,
                    HarvestFrom = vegetable.HarvestFrom,
                    HarvestTo = vegetable.HarvestTo,
                    Description = vegetable.Description,
                    Thumbnail = vegetable.Thumbnail,
                    IsDelete = vegetable.IsDelete
                };
            }

            return null;
        }
        #endregion

        #region Delete vegetable
        public bool DeleteVegetable(int id, bool mode)
        {
            var vegetable = _uow.Vegetable.GetById(id);

            if (vegetable != null)
            {
                vegetable.IsDelete = mode;

                _uow.Commit();

                return true;
            }

            return false;
        }
        #endregion

        #region Detect and send notification

        public bool DetectAndSendNoti(string username)
        {
            var listVegetable = _service.CallScriptPython().ToList();
            if (listVegetable != null)
            {
                var listPlotId = new List<int>();
                foreach (VegetableModel detectionVegetable in listVegetable)
                {
                    Pot pot = _uow.Pot.Get().Where(p => p.Position == detectionVegetable.Pot).FirstOrDefault();
                    PotVegetable potVegetable = _uow.PotVegetable.GetPotVegetableWithPotID(pot.Id);
                    // neu pot vegetable == null
                    // thi o vi tri nay chua co cay nao duoc trong
                    if (potVegetable != null)
                    {
                        // Find disease with labelId
                        foreach (int labelId in detectionVegetable.listDisease)
                        {
                            if (!listPlotId.Contains(pot.PlotId))
                            {
                                listPlotId.Add(pot.PlotId);
                            }
                            Disease disease = _uow.Disease.GetByLabelId(labelId);
                            // Find lastest PotVegetableDisease
                            PotVegetableDisease pvdLastest = _uow.PotVegetableDisease.GetLastest(potVegetable.Id, disease.Id);
                            int flag = 0; // 0 is create new
                            flag += pvdStatus(pvdLastest, PotVegetableDiseaseStatus.UN_RESOLVE);
                            if (pvdLastest != null && pvdLastest.Status == PotVegetableDiseaseStatus.DELAY)
                            {
                                var plant = potVegetable.DatePlant;
                                var now = DateTime.Now;
                                flag++;
                            }
                            if (flag == 0)
                            {
                                PotVegetableDisease pvd = createPotVegetableDisease(potVegetable.Id, disease.Id);
                            }
                        }
                        // Con sau
                        if (detectionVegetable.ConSau > 0) // > 0 co sau
                        {
                            potVegetable.HasWorm = true;

                        }
                        potVegetable.UrlImage = detectionVegetable.UrlImage;
                    }

                    if (pot.Status == PotStatus.BLANK && detectionVegetable.CayCon != 0)
                    {
                        int vegetableId = (detectionVegetable.CayCon == 4 ? 1 : 4);
                        // add new potVegetable
                        var potVeg = _uow.PotVegetable.Get().Where(p => p.PotId == pot.Id && p.Status == PotVegetableStatus.PENDING && p.IsDelete == false).FirstOrDefault();
                        if (potVeg == null)
                        {
                            var newPotVegetable = new PotVegetable()
                            {
                                PotId = pot.Id,
                                VegetableId = vegetableId,
                                // trừ 7 ngày
                                DatePlant = DateTime.Now.AddDays(-7),
                                Status = PotVegetableStatus.PENDING,
                                UrlImage = detectionVegetable.UrlImage,
                                HasWorm = false,
                                IsDelete = false
                            };
                            _uow.PotVegetable.Create(newPotVegetable);
                        }
                    }
                }
                // create notification
                foreach (var item in listPlotId)
                {
                    var notification = new Notification()
                    {
                        Username = username,
                        PlotId = item,
                        Description = "Luống rau số " + _uow.Plot.Get().Where(p => p.Id == item).FirstOrDefault().Line + " của bạn đang có vấn đề về sâu bệnh",
                        DateTime = DateTime.Now,
                        IsSeen = false,
                        IsDelete = false
                    };
                    _uow.Notification.Create(notification);
                }
                _uow.Commit();
                _serviceNoti.SendNotification(username, (listPlotId.Count > 0 ? 1 : 0));
                return true;
            }
            else
            {
                return false;
            }
        }

        private int pvdStatus(PotVegetableDisease pvd, int status)
        {
            return pvd != null && pvd.Status == status ? 1 : 0;
        }

        private PotVegetableDisease createPotVegetableDisease(int potVegetableId, int diseaseId)
        {
            PotVegetableDisease potVegetableDisease = new PotVegetableDisease
            {
                PotVegetableId = potVegetableId,
                DiseaseId = diseaseId,
                DateBegin = DateTime.Now,
                Status = PotVegetableDiseaseStatus.UN_RESOLVE
            };
            _uow.PotVegetableDisease.Create(potVegetableDisease);

            return potVegetableDisease;
        }
        #endregion

        #region Get all pot vegetable information
        public IEnumerable<PotVegetableInfo> GetAllPotVegetableInfo(string username)
        {
            var pots = _uow.Pot.GetUserListPots(username);
            var test = pots.Count();
            var listResult = new List<PotVegetableInfo>();
            foreach (var pot in pots)
            {
                Vegetable vegetable = null;
                List<int> listDisease = null;
                bool resolve = true;
                string dateHarvest = "";
                string description = "";
                var potVegetable = _uow.PotVegetable.Get().Where(p => p.PotId == pot.Id && p.Status != PotVegetableStatus.HARVESTED && p.IsDelete == false).OrderByDescending(p => p.DatePlant).FirstOrDefault();
                if (potVegetable != null)
                {
                    vegetable = _uow.Vegetable.GetVegetableById(potVegetable.VegetableId);
                    listDisease = _uow.PotVegetableDisease.Get().Where(p => p.PotVegetableId == potVegetable.Id && p.Status != 1).Select(p => p.DiseaseId).ToList();
                    resolve = (_uow.PotVegetableDisease.Get().Where(p => p.PotVegetableId == potVegetable.Id && p.Status == PotVegetableDiseaseStatus.UN_RESOLVE).ToList().Count > 0 ? false:true);
                    var averageDay = (vegetable.HarvestFrom + vegetable.HarvestTo) / 2;
                    dateHarvest = (potVegetable.DatePlant.Value.AddDays(averageDay)).ToShortDateString();
                    if(potVegetable.HasWorm == true)
                    {
                        description = description + "Bạn hãy bắt con sâu đó ra để nó ăn lá";
                    }
                    if(potVegetable.Status == PotVegetableStatus.PENDING)
                    {
                        description = description + "Đã phát hiện cây con ở giai đoạn 1 ( 7 ngày )";
                    }
                }

           
                if (listDisease != null)
                {
                    foreach (var item in listDisease)
                    {
                        if (item == 1 || item == 2)
                        {
                            description = "Bạn nên ngắt lá đó đi , để tránh ảnh hưởng sức khỏe của cây,";
                        }
                        if (item == 7)
                        {
                            description = description + "Hãy ngắt là vàng của bạn ra , để tránh ảnh hưởng sức khỏe của cây";
                        }
                        if (item == 8)
                        {
                            description = description + "Cây của bạn đã bị chết , hãy đem ra khỏi giàn";
                        }
                    }
                }
                var result = new PotVegetableInfo()
                {
                    PotId = pot.Id,
                    VegetableId = vegetable == null ? 0 : vegetable.Id,
                    Position = pot.Position.Value,
                    PlotId = pot.PlotId,
                    DateHarvest = dateHarvest,
                    Name = vegetable == null ? null : vegetable.Name,
                    UrlImage = potVegetable == null ? null : potVegetable.UrlImage,
                    HasWorm = potVegetable == null ? false : potVegetable.HasWorm,
                    ListDiseases = listDisease,
                    Discription = description,
                    Status = potVegetable == null ? 0 : potVegetable.Status,
                    potVegetableId = potVegetable == null ? 0 : potVegetable.Id,
                    Resolve = resolve,

                };
                listResult.Add(result);
            }
            return listResult;

        }
        #endregion

        #region Resolve disease

        public bool ResolveDisease(int potVetableId)
        {
            var potsVegetableDisease = _uow.PotVegetableDisease.Get().Where(p => p.PotVegetableId == potVetableId && p.Status == PotVegetableDiseaseStatus.UN_RESOLVE && p.IsDelete == false).ToList();
            var potVegetable = _uow.PotVegetable.Get().Where(p => p.Id == potVetableId && p.IsDelete == false && p.DateHarvest == null).FirstOrDefault();
            if (potVegetable != null)
            {
                potVegetable.HasWorm = false;
            }

            if (potsVegetableDisease.Count > 0)
            {
                foreach (var item in potsVegetableDisease)
                {
                    item.Status = PotVegetableDiseaseStatus.RESOLVED;
                }
                _uow.Commit();
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion
        #region Confirm young vegetable
        public bool ConfirmYoungVegetable(int potId, int day)
        {
            var potVegetable = _uow.PotVegetable.Get().Where(p=>p.PotId == potId && p.Status == PotVegetableStatus.PENDING && p.IsDelete == false).FirstOrDefault();
            if (potVegetable != null)
            {
                potVegetable.Status = PotVegetableStatus.PLANTING;
                var pot = _uow.Pot.Get().Where(p => p.Id == potId && p.IsDelete == false).FirstOrDefault();
                pot.Status = PotStatus.PLANT;
                if (day != 7)
                {
                    // update date plant
                    potVegetable.DatePlant = DateTime.Now.AddDays(-day);
                }
                _uow.Commit();
                return true;
            }
            else
            {
                return false;
            }

        }
        #endregion
    }
}
