﻿using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Security;
using DataService.DesignPatern.ServiceModels;
using DataService.DesignPatern.UnitOfWorks;
using DataService.Models;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.Services
{
    public interface IImageDiseaseService
    {
        ImageDiseaseServiceModel Create(CreateImageDiseaseRequestModel model);
        IEnumerable<ImageDiseaseServiceModel> GetAll(int size = 10, int page = 1);
    }

    public class ImageDiseaseService : BaseUnitOfWork<UnitOfWork>, IImageDiseaseService
    {
        private readonly AppSettings _appSettings;
        public ImageDiseaseService(UnitOfWork uow, IOptions<AppSettings> appSettings) : base(uow)
        {
            _appSettings = appSettings.Value;
        }

        #region Get all
        public IEnumerable<ImageDiseaseServiceModel> GetAll(int size = 10, int page = 1)
        {
            var images = _uow.ImageDisease.GetAll(size, page);

            if (images != null && images.Any())
            {
                List<ImageDiseaseServiceModel> list = new List<ImageDiseaseServiceModel>();
                foreach (var img in images)
                {
                    Vegetable veg = _uow.Vegetable.GetById(img.VegetableId);
                    if (veg != null)
                    {
                        list.Add(new ImageDiseaseServiceModel
                        {
                            id = img.Id,
                            VegetableId = img.VegetableId,
                            Vegetable = veg.Name,
                            Path = img.Path,
                            FeedbackContent = img.FeedbackContent,
                            IsDelete = false
                        });
                    }
                }

                return list;
            }

            return null;
        }
        #endregion

        #region Create new image
        public ImageDiseaseServiceModel Create(CreateImageDiseaseRequestModel model)
        {
            Vegetable vegetable = _uow.Vegetable.GetById(model.VegetableId);

            if (vegetable != null && vegetable.IsDelete == false)
            {
                ImageDisease imageDisease = new ImageDisease
                {
                    VegetableId = model.VegetableId,
                    Path = model.Path,
                    FeedbackContent = model.FeedbackContent,
                    IsDelete = false
                };

                _uow.ImageDisease.Create(imageDisease);
                _uow.Commit();

                return new ImageDiseaseServiceModel
                {
                    id = imageDisease.Id,
                    VegetableId = imageDisease.VegetableId,
                    Path = imageDisease.Path,
                    FeedbackContent = imageDisease.FeedbackContent,
                    IsDelete = false
                };
            }

            return null;
        }
        #endregion
    }
}
