﻿using DataService.DesignPatern.Enum;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Security;
using DataService.DesignPatern.ServiceModels;
using DataService.DesignPatern.UnitOfWorks;
using DataService.Models;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.Services
{
    public interface IContractService
    {
        Contract GetContractByID(int id);
        ContractServiceModel CreateNewContract(CreateContractRequestModel model);
        ContractServiceModel UpdateContract(int id, UpdateContractRequestModel model);
        bool DeleteContract(int id, bool mode);
        IEnumerable<Contract> GetContracts(int size = 10, int page = 1);
        Contract PauseContract(int id);
    }
    public class ContractService : BaseUnitOfWork<UnitOfWork>, IContractService
    {
        private readonly AppSettings _appSettings;
        public ContractService(UnitOfWork uow, IOptions<AppSettings> appSettings) : base(uow)
        {
            _appSettings = appSettings.Value;
        }

        #region Get contracts
        public IEnumerable<Contract> GetContracts(int size = 10, int page = 1)
        {
            var contracts = _uow.Contract.GetContracts(size, page);
            var result = new List<Contract>();

            if (contracts != null && contracts.Any())
            {
                foreach (var contract in contracts)
                {
                    int? contractStatus = contract.Status;
                    if (contractStatus != ContractStatus.PAUSED)
                    {
                        var date = contract.Duration.Value;

                        var today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                        var endDate = new DateTime(date.Year, date.Month, date.Day);
                        // check expried or not
                        contractStatus = DateTime.Compare(today, endDate) <= 0 ? ContractStatus.ONGOING : ContractStatus.EXPRIED;
                    }
                    contract.Status = contractStatus;
                    result.Add(contract);
                }
                _uow.Commit();

                return result ?? null;
            }

            return null;
        }
        #endregion

        #region Get contract by id
        public Contract GetContractByID(int id)
        {
            var contract = _uow.Contract.GetContractByID(id);

            return contract ?? null;
        }
        #endregion

        #region Create new contract
        public ContractServiceModel CreateNewContract(CreateContractRequestModel model)
        {
            var userUnexist = _uow.User.CheckUsername(model.Username);
            
            if (!userUnexist && _uow.Contract.IsAvailableNewContract(model.Username))
            {
                var contract = new Contract
                {
                    Username = model.Username,
                    DateCreate = DateTime.Now,
                    DateStart = model.DateStart,
                    Duration = model.Duration,
                    Fee = model.Fee,
                    Status = ContractStatus.ONGOING
                };

                _uow.Contract.Create(contract);
                _uow.Commit();

                return new ContractServiceModel
                {
                    ID = contract.Id,
                    Username = contract.Username,
                    DateCreate = contract.DateCreate,
                    DateStart = model.DateStart,
                    Duration = model.Duration,
                    Fee = model.Fee,
                    Status = ContractStatus.ONGOING
                };
            }

            return null;
        }
        #endregion

        #region Update Contract
        public ContractServiceModel UpdateContract(int id, UpdateContractRequestModel model)
        {
            var contract = _uow.Contract.GetContractByID(id);

            if (contract != null)
            {
                contract.DateCreate = model.DateCreate;
                contract.DateStart = model.DateStart;
                contract.Duration = model.Duration;
                contract.Fee = model.Fee;
                contract.IsDelete = model.IsDelete;

                _uow.Commit();

                return new ContractServiceModel
                {
                    ID = contract.Id,
                    Username = contract.Username,
                    DateCreate = model.DateCreate,
                    DateStart = model.DateStart,
                    Duration = model.Duration,
                    Fee = model.Fee,
                    IsDelete = model.IsDelete
                };
            }

            return null;
        }
        #endregion

        #region Delete Contract
        public bool DeleteContract(int id, bool mode)
        {
            var contract = _uow.Contract.GetContractByID(id);

            if (contract != null)
            {
                contract.IsDelete = mode;

                _uow.Commit();

                return true;
            }

            return false;
        }
        #endregion

        #region Pause contract
        public Contract PauseContract(int id)
        {
            var contract = _uow.Contract.GetContractByID(id);

            if (contract != null)
            {
                contract.Status = ContractStatus.PAUSED;
                _uow.Commit();

                return contract;
            }

            return null;
        }
        #endregion
    }
}
