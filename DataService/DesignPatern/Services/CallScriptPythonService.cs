﻿using DataService.DesignPatern.Enum;
using DataService.DesignPatern.UnitOfWorks;
using DataService.Models;
using DataService.Models.ModelCreate;
using Microsoft.Scripting.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Services
{

    public class CallScriptPythonService : BaseUnitOfWork<UnitOfWork>
    {
        string coordinatesDir = @"D:\Huangshan\Projects\Study\Capsone\Coordinate";
        string pythonDir = @"C:\Program Files\WindowsApps\PythonSoftwareFoundation.Python.3.7_3.7.2288.0_x64__qbz5n2kfra8p0";

        private readonly NotificationFireBaseService _service;
        public CallScriptPythonService(UnitOfWork uow, NotificationFireBaseService service) : base(uow)
        {
            _service = service;
        }
        public List<VegetableModel> CallScriptPython()
        {
            try
            {
                var psi = new ProcessStartInfo();
                psi.FileName = @"C:\Users\DELL\AppData\Local\Programs\Python\Python37\python.exe";
                var script = @"D:\testCapstone\Capstone-Vegetable\detectDisease.py";
                psi.Arguments = script;
                //Console.WriteLine(psi.Arguments);
                psi.UseShellExecute = false;
                psi.CreateNoWindow = true;
                psi.RedirectStandardOutput = true;
                psi.RedirectStandardError = true;
                var error = "";
                var result = "";
                Console.WriteLine(error);
                using (var process = Process.Start(psi))
                {
                    error = process.StandardError.ReadToEnd();
                    result = process.StandardOutput.ReadToEnd();
                }

                var vegetableObj = JsonConvert.DeserializeObject<List<VegetableModel>>(result);           
                return vegetableObj;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public string GetCoordinate()
        {
            try
            {
                var psi = new ProcessStartInfo();
                psi.FileName = pythonDir + @"\python.exe";
                var script = coordinatesDir + @"\getxy.py";
                psi.Arguments = script;
                //Console.WriteLine(psi.Arguments);
                psi.UseShellExecute = false;
                psi.CreateNoWindow = true;
                psi.RedirectStandardOutput = true;
                psi.RedirectStandardError = true;
                var error = "";
                var result = "";
                Console.WriteLine(error);
                using (var process = Process.Start(psi))
                {
                    error = process.StandardError.ReadToEnd();
                    result = process.StandardOutput.ReadToEnd();
                }
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public string GetImageFromIP()
        {
            try
            {
                var psi = new ProcessStartInfo();
                psi.FileName = pythonDir + @"\python.exe";
                var script = coordinatesDir + @"\getImageFromIp.py";
                psi.Arguments = script;
                //Console.WriteLine(psi.Arguments);
                psi.UseShellExecute = false;
                psi.CreateNoWindow = true;
                psi.RedirectStandardOutput = true;
                psi.RedirectStandardError = true;
                var error = "";
                var result = "";
                Console.WriteLine(error);
                using (var process = Process.Start(psi))
                {
                    error = process.StandardError.ReadToEnd();
                    result = process.StandardOutput.ReadToEnd();
                }
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }


        private Notification createNotification(string username, int plotId)
        {
            Notification notification = new Notification
            {
                Username = username,
                PlotId = plotId,
                DateTime = DateTime.Now,
                Description = "This plant has problem"
            };

            _uow.Notification.Create(notification);

            return notification;
        }

     
    }
}
