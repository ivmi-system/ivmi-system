﻿using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Security;
using DataService.DesignPatern.ServiceModels;
using DataService.DesignPatern.UnitOfWorks;
using DataService.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Services
{

    public interface INotificationService
    {
        IEnumerable<Notification> GetListNotifications(int size = 10, int page = 1);
        NotificationServiceModel CreateNewNotification(CreateNotificationRequestModel model);
        bool UpdateSeenNotification(int id);
        bool DeleteNotification(int id, bool mode);
    }
    public class NotificationService : BaseUnitOfWork<UnitOfWork>, INotificationService
    {
        private readonly AppSettings _appSettings;
        public NotificationService(UnitOfWork uow, IOptions<AppSettings> appSettings) : base(uow)
        {
            _appSettings = appSettings.Value;
        }

        #region Get list notification
        public IEnumerable<Notification> GetListNotifications(int size = 10, int page = 1)
        {
            IEnumerable<Notification> notifications = _uow.Notification.GetListNotifications(size, page);

            if (notifications != null)
            {
                return notifications;
            }

            return null;
        }

        public static implicit operator NotificationService(VegetableService v)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Create new notification
        public NotificationServiceModel CreateNewNotification(CreateNotificationRequestModel model)
        {
            bool userUnexit = _uow.User.CheckUsername(model.UserName);

            if (userUnexit == false)
            {
                var plot = _uow.Plot.GetUserPlotByID(model.UserName, model.PlotID);
                if (plot != null)
                {
                    var notification = new Notification
                    {
                        Username = model.UserName,
                        PlotId = model.PlotID,
                        Description = model.Description,
                        DateTime = model.DateTime,
                        IsSeen = model.IsSeen,
                        IsDelete = model.IsDelete
                    };

                    _uow.Notification.Create(notification);
                    _uow.Commit();

                    return new NotificationServiceModel
                    {
                        ID = notification.Id,
                        UserName = notification.Username,
                        PlotID = notification.PlotId,
                        Description = notification.Description,
                        DateTime = notification.DateTime,
                        IsSeen = notification.IsSeen,
                        IsDelete = notification.IsDelete
                    };
                }
            }

            return null;
        }
        #endregion

        #region Update seen notification
        public bool UpdateSeenNotification(int id)
        {
            var noti = _uow.Notification.Get().Where(p=> p.Id == id && p.IsDelete == false && p.IsSeen == false).FirstOrDefault();

            if (noti != null)
            {
                noti.IsSeen = true;
                _uow.Commit();
                return true;
            }

            return false;
        }
        #endregion

        #region Delete Notification
        public bool DeleteNotification(int id, bool mode)
        {
            var notification = _uow.Notification.GetById(id);

            if (notification != null)
            {
                notification.IsDelete = mode;

                _uow.Commit();

                return true;
            }

            return false;
        }
        #endregion
    }
}
