﻿using DataService.DesignPatern.Enum;
using DataService.DesignPatern.Handler;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Security;
using DataService.DesignPatern.ServiceModels;
using DataService.DesignPatern.UnitOfWorks;
using DataService.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace DataService.DesignPatern.Services
{
    public interface IUserService
    {
        LoginServiceModel CheckLogin(LoginRequestModel model);
        IEnumerable<User> GetAllUser(string currentUsername);
        IEnumerable<User> GetAllUser(string currentUsername, int size, int page);
        bool UpdateDeviceToken(string username, string deviceToken);
        UserServiceModel CreateAccount(CreateUserRequestModel model);
        UserServiceModel GetUserByUsername(string username);
        User UpdateUser(string username, UpdateUserRequestModel model);
        IEnumerable<ScheduleHandlerServiceModel> GetUserSchedules(string username);
        Schedule GetUserScheduleByID(string username, int id);
        Plot GetUserPlotByID(string username, int id);
        IEnumerable<PlotServiceModel> GetUserPlots(string username, bool show_pots = false);
        IEnumerable<Notification> GetUserNotifications(string username, int size, int page);
        IEnumerable<Notification> GetUserNotifications(string username, bool isSeen, int size, int page);
        IEnumerable<Contract> GetUserContracts(string username, int size = 10, int page = 1, bool lastest = false);
        bool MarkAsReadNotifications(string username);
        bool StartUserSchedule(string username);
        bool StopUserSchedule(string username);
        Contract GetLastestDurationUser(string username);
        bool IsAvailableNewContract(string username);
    }
    public class UserService : BaseUnitOfWork<UnitOfWork>, IUserService
    {
        private readonly AppSettings _appSettings;
        public VegetableService _service;

        public UserService(UnitOfWork uow, IOptions<AppSettings> appSettings, VegetableService service) : base(uow)
        {
            _appSettings = appSettings.Value;
            _service = service;
        }

        #region Login
        public LoginServiceModel CheckLogin(LoginRequestModel model)
        {
            // end code MD5 to check login
            var passwordMD5 = EncryptorService.MD5Hash(model.Password);
            var user = _uow.User.CheckLogin(model.Username, passwordMD5);
            if (user == null)
            {
                return null;
            }
            else
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim(ClaimTypes.Name, user.Username.ToString()),
                    new Claim(ClaimTypes.Role,user.Role.ToString())

                    }),
                    // Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);

                // Lay contract info
                int? contractStatus = ContractStatus.NOT_YET;

                var contract = _uow.Contract.Get().Where(p => p.IsDelete == false).FirstOrDefault();
                
                if (contract != null)
                {
                    contractStatus = contract.Status;
                    if (contractStatus != ContractStatus.PAUSED)
                    {
                        var date = contract.Duration.Value;

                        var today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                        var endDate = new DateTime(date.Year, date.Month, date.Day);
                        // check expried or not
                        contractStatus = DateTime.Compare(today, endDate) <= 0 ? ContractStatus.ONGOING : ContractStatus.EXPRIED;
                    }
                }

                var result = new LoginServiceModel
                {
                    Username = user.Username,
                    Token = tokenHandler.WriteToken(token),
                    Role = user.Role,
                    ContractStatus = contractStatus
                };

                // re-update contract if expried or not
                if (contract != null && contract.Status != ContractStatus.PAUSED)
                {
                    contract.Status = contractStatus;
                    _uow.Commit();
                }

                return result;
            }
        }
        #endregion

        #region Get all user
        public IEnumerable<User> GetAllUser(string currentUsername)
        {
            var users = _uow.User.GetAllUser(currentUsername);

            if (users != null && users.Any())
            {
                return users;
            }


            return null;
        }

        public IEnumerable<User> GetAllUser(string currentUsername, int size, int page)
        {
            var result = _uow.User.GetAllUser(currentUsername, size, page);
            if (result == null)
            {
                return null;
            }
            else
            {
                return result;
            }
        }
        #endregion

        #region Create Account
        public UserServiceModel CreateAccount(CreateUserRequestModel model)
        {
            var isUnexistUsername = _uow.User.CheckUsername(model.Username);

            if (isUnexistUsername)
            {
                var passwordMd5 = EncryptorService.MD5Hash(model.Password);
                var user = new User
                {
                    Username = model.Username,
                    Name = model.Name,
                    Password = passwordMd5,
                    Phone = model.Phone,
                    Email = model.Email,
                    Address = model.Address,
                    CameraIp = model.CameraIp,
                    Role = Role.USER,
                    IsDelete = false
                };
                _uow.User.Create(user);
                _uow.Commit();

                // Create 2 default schedule on am and pm
                CreateNewUserSchedule(ScheduleEnum.DEFAULT_AM, user.Username);
                CreateNewUserSchedule(ScheduleEnum.DEFAULT_PM, user.Username);

                return new UserServiceModel 
                { 
                    Username = user.Username,
                    Name = user.Name,
                    Phone = user.Phone,
                    Email = user.Email,
                    Address = user.Address,
                    CameraIp = user.CameraIp,
                    Role = user.Role,
                    IsDelete = user.IsDelete
                };
            }

            return null;
        }
        #endregion

        #region Create new default schedule time for create new user
        private void CreateNewUserSchedule(TimeSpan time, string username)
        {
            Schedule schedule = new Schedule
            {
                Username = username,
                Time = time,
                IsDelete = false
            };

            _uow.Schedule.Create(schedule);
            _uow.Commit();
        }
        #endregion

        #region Update Device Token
        public bool UpdateDeviceToken(string username, string deviceToken)
        {
            var user = _uow.User.GetUserByUsername(username, false);
            if (user != null)
            {
                user.DeviceToken = deviceToken;
                _uow.Commit();
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Block/Unblock Account
        public bool BlockAndUnBlockAccount(string username, int mode)
        {
            var user = _uow.User.Get().Where(p => p.Username == username).FirstOrDefault();
            if (user != null)
            {
                // block
                if (mode == UserEnum.BLOCK)
                {
                    user.IsDelete = true;
                    _uow.Commit();
                    return true;
                }
                // unblock
                if (mode == UserEnum.UNBLOCK)
                {
                    user.IsDelete = false;
                    _uow.Commit();
                    return true;
                }
                return false;

            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Get user by username
        public UserServiceModel GetUserByUsername(string username)
        {
            var user = _uow.User.GetUserByUsername(username);
            if (user != null)
            {
                return new UserServiceModel
                {
                    Username = user.Username,
                    Name = user.Name,
                    DeviceToken = user.DeviceToken,
                    Phone = user.Phone,
                    Address = user.Address,
                    Email = user.Email,
                    CameraIp = user.CameraIp,
                    Role = user.Role,
                    IsDelete = user.IsDelete
                };
            }

            return null;
        }
        #endregion

        #region Update User
        public User UpdateUser(string username, UpdateUserRequestModel model)
        {
            User user = _uow.User.GetUserByUsername(username);

            if (user != null)
            {
                if (model.Password != null)
                {
                    var passwordMd5 = EncryptorService.MD5Hash(model.Password);
                    user.Password = passwordMd5;
                }

                if (model.Name != null)
                    user.Name = model.Name;

                if (model.DeviceToken != null)
                    user.DeviceToken = model.DeviceToken;

                if (model.Phone != null)
                    user.Phone = model.Phone;

                if (model.Email != null)
                {
                    user.Email = model.Email;
                }

                if (model.Address != null)
                {
                    user.Address = model.Address;
                }

                if (model.CameraIp != null)
                {
                    user.CameraIp = model.CameraIp;
                }

                _uow.Commit();

                return user;
            }

            return null;
        }
        #endregion

        #region Get user schedule
        public IEnumerable<ScheduleHandlerServiceModel> GetUserSchedules(string username)
        {
            IEnumerable<Schedule> schedules = _uow.Schedule.GetUserSchedules(username);

            if (schedules != null && schedules.Any())
            {
                List<ScheduleHandlerServiceModel> list = new List<ScheduleHandlerServiceModel>();

                foreach (Schedule schedule in schedules)
                {
                    ScheduleHandlerServiceModel model = new ScheduleHandlerServiceModel
                    {
                        ID = schedule.Id,
                        Username = schedule.Username,
                        Time = schedule.Time,
                        IsDelete = schedule.IsDelete,
                        IsRunning = new HandlerJob { JobName = EncryptorService.MD5Hash(schedule.Username + "_" + schedule.Id) }.AnyJob()
                    };

                    list.Add(model);
                }

                return list;
            }

            return null;
        }
        #endregion

        #region Get user schedule by id
        public Schedule GetUserScheduleByID(string username, int id)
        {
            Schedule schedule = _uow.Schedule.GetUserScheduleByID(username, id);

            if (schedule != null)
            {
                return schedule;
            }

            return null;
        }
        #endregion

        #region Get user's plot by id
        public Plot GetUserPlotByID(string username, int id)
        {
            Plot plot = _uow.Plot.GetUserPlotByID(username, id);

            if (plot != null)
            {
                return plot;
            }

            return null;
        }
        #endregion

        #region Get user's plots
        public IEnumerable<PlotServiceModel> GetUserPlots(string username, bool show_pots = false)
        {
            IEnumerable<Plot> plots = _uow.Plot.GetUserPlots(username);

            List<PlotServiceModel> listPlotServiceModels = new List<PlotServiceModel>();
            foreach (Plot plot in plots)
            {
                listPlotServiceModels.Add(new PlotServiceModel
                {
                    ID = plot.Id,
                    Username = plot.Username,
                    Line = plot.Line,
                    Size = plot.Size,
                    IsDelete = plot.IsDelete,
                });
            }

            IEnumerable<PlotServiceModel> plotServiceModels = listPlotServiceModels;
            if (plots != null && plots.Any())
            {
                System.Diagnostics.Debug.WriteLine("1: " + show_pots);
                if (show_pots)
                {
                    foreach (PlotServiceModel plotServiceModel in plotServiceModels)
                    {
                        IEnumerable<PotServiceModel> pots = _uow.Pot.GetAllPotByPlotID(plotServiceModel.ID);
                        System.Diagnostics.Debug.WriteLine("2 num_pots: " + pots.Count() + plotServiceModel.ID);
                        if (pots != null && pots.Any())
                        {
                            System.Diagnostics.Debug.WriteLine("3 num_pots: " + pots.Count());
                            plotServiceModel.Pots = pots;
                        }
                    }
                }
                return plotServiceModels;
            }
            return null;
        }
        #endregion

        #region Get user's notifications
        public IEnumerable<Notification> GetUserNotifications(string username, int size, int page)
        {
            var notifications = _uow.Notification.GetUserNotifications(username, size, page);

            if (notifications != null && notifications.Any())
            {
                return notifications.OrderByDescending(p=>p.DateTime);
            }

            return null;
        }

        public IEnumerable<Notification> GetUserNotifications(string username, bool isSeen, int size, int page)
        {
            var notifications = _uow.Notification.GetUserNotifications(username, isSeen, size, page);

            if (notifications != null && notifications.Any())
            {
                return notifications;
            }

            return null;
        }
        #endregion

        #region Get user's contracts
        public IEnumerable<Contract> GetUserContracts(string username, int size = 10, int page = 1, bool lastest = false)
        {
            var contracts = _uow.Contract.GetUserContracts(username, size, page, lastest);

            if (contracts != null && contracts.Any())
            {
                return contracts;
            }

            return null;
        }
        #endregion

        #region Mark as read all notifications of user
        public bool MarkAsReadNotifications(string username)
        {
            var notifications = _uow.Notification.GetUserUnreadNotification(username);

            if (notifications != null && notifications.Any())
            {
                foreach (Notification noti in notifications)
                {
                    noti.IsSeen = true;
                    System.Diagnostics.Debug.Write(noti);
                }
                _uow.Commit();

                var result = notifications.Select(noti => noti.IsSeen == true).ToList();
                return result.Count() == notifications.Count();
            }

            return false;
        }
        #endregion

        #region Start User Schedule
        public bool StartUserSchedule(string username)
        {
            var count = 0;
            var schedules = _uow.Schedule.GetUserSchedules(username);

            if (schedules != null && schedules.Any())
            {
                foreach (Schedule schedule in schedules)
                {
                    var jobName = EncryptorService.MD5Hash(username + "_" + schedule.Id);
                    var handlerJob = new HandlerJob
                    {
                        Time = schedule.Time,
                        JobName = jobName,
                        Username = schedule.Username,
                        _service = _service,
                    };

                    handlerJob.StartJob();

                    count += handlerJob.AnyJob() ? 1 : 0;
                }

                // System.Diagnostics.Debug.WriteLine(count);

                return count == schedules.Count();
            }

            return false;
        }
        #endregion

        #region Stop user schedules
        public bool StopUserSchedule(string username)
        {
            var count = 0;
            var schedules = _uow.Schedule.GetUserSchedules(username);

            if (schedules != null && schedules.Any())
            {
                foreach (Schedule schedule in schedules)
                {
                    var jobName = EncryptorService.MD5Hash(username + "_" + schedule.Id);
                    var handlerJob = new HandlerJob();
                    handlerJob.StopJob(jobName);

                    count += !handlerJob.AnyJob() ? 1 : 0;
                }

                System.Diagnostics.Debug.WriteLine(count);

                return count == schedules.Count();
            }

            return false;
        }
        #endregion

        #region Get user contract by lastest duration
        public Contract GetLastestDurationUser(string username)
        {
            var contract = _uow.Contract.GetLastestDurationUser(username);

            if (contract != null)
            {
                return contract;
            }

            return null;
        }
        #endregion

        #region Check user can create new contract or not
        public bool IsAvailableNewContract(string username)
        {
            return _uow.Contract.IsAvailableNewContract(username);
        }
        #endregion
    }
}

