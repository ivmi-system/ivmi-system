﻿using DataService.DesignPatern.Enum;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Security;
using DataService.DesignPatern.ServiceModels;
using DataService.DesignPatern.UnitOfWorks;
using DataService.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Services
{
    public interface IPotService
    {
        IEnumerable<Pot> GetListPots(int size = 10, int page = 1);
        PotServiceModel CreateNewPot(CreatePotRequestModel model);
        PotServiceModel UpdatePot(int id, UpdatePotRequestModel model);
        bool DeletePot(int id, bool mode);
        Pot GetPotWithUsernameAndPosition(string username, int position);
        //bool UpdateCoordinates(int id, string coordinates);
        bool UpdateCoordinate(int id);
        bool Harvest(int id);
    }
    public class PotService : BaseUnitOfWork<UnitOfWork>, IPotService
    {
        private readonly CallScriptPythonService _callScriptService;
        public PotService(UnitOfWork uow, CallScriptPythonService callScriptService) : base(uow)
        {     
            _callScriptService = callScriptService;
        }

        #region Get list pot
        public IEnumerable<Pot> GetListPots(int size = 10, int page = 1)
        {
            IEnumerable<Pot> pots = _uow.Pot.GetListPots(size, page);

            if (pots != null)
            {
                return pots;
            }

            return null;
        }
        #endregion

        #region Create new pot
        public PotServiceModel CreateNewPot(CreatePotRequestModel model)        {

            var plot = _uow.Plot.GetById(model.PlotID);
            if (plot == null)
            {
                return null;
            }
            else
            {
                var pot = new Pot
                {
                    PlotId = model.PlotID,
                    Position = model.Position,
                    Status = model.Status,
                    IsDelete = model.IsDelete
                };

                _uow.Pot.Create(pot);
                _uow.Commit();

                return new PotServiceModel
                {
                    ID = pot.Id,
                    PlotID = pot.PlotId,
                    Position = pot.Position,
                    Status = pot.Status,                    
                    IsDelete = pot.IsDelete
                };
            }            
        }
        #endregion

        #region Update pot
        public PotServiceModel UpdatePot(int id, UpdatePotRequestModel model)
        {
            Pot pot = _uow.Pot.GetById(id);
            
            if (pot != null)
            {

                pot.Position = model.Position;
                pot.Status = model.Status;

                if (model.Coordinates != null)
                {
                    pot.Coordinates = model.Coordinates;
                }

                _uow.Commit();
                return new PotServiceModel
                {
                    ID = pot.Id,
                    Position = pot.Position,
                    Status = pot.Status,
                    IsDelete = pot.IsDelete
                };
            }
            return null;
        }
        #endregion

        #region Delete pot
        public bool DeletePot(int id, bool mode)
        {
            var pot = _uow.Pot.GetById(id);

            if (pot != null)
            {
                pot.IsDelete = mode;

                _uow.Commit();

                return true;
            }

            return false;
        }
        #endregion

        #region Get Pot with username and pot.position
        public Pot GetPotWithUsernameAndPosition(string username, int position)
        {
            Pot pot = _uow.Pot.GetPotWithUsernameAndPosition(username, position);

            if (pot != null)
            {
                return pot;
            }

            return null;
        }
        #endregion

        #region Get user list pots
        public IEnumerable<Pot> GetUserListPots(string username)
        {
            var pots = _uow.Pot.GetUserListPots(username).OrderBy(p => p.Position);

            if (pots != null && pots.Any())
            {
                return pots;
            }

            return null;
        }
        #endregion

        //#region Update coordinates
        //public bool UpdateCoordinates(int id, string coordinates)
        //{
        //    Pot pot = _uow.Pot.GetById(id);
        //    if (pot != null && coordinates != null)
        //    {
        //        pot.Coordinates = coordinates;
        //        _uow.Commit();

        //        return pot.Coordinates == coordinates;
        //    }

        //    return false;
        //}
        //#endregion
        #region Update coordinate
        public bool UpdateCoordinate(int id)
        {
            string result = _callScriptService.GetCoordinate();
            if (!string.IsNullOrEmpty(result))
            {
                var pot = _uow.Pot.Get().Where(p => p.Id == id && p.IsDelete == false).FirstOrDefault();
                pot.Coordinates = result;
                _uow.Commit();
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region Harvest
        public bool Harvest(int id)
        {
            Pot pot = _uow.Pot.GetByID(id);
            if (pot != null && pot.IsDelete == false && pot.Status == PotStatus.PLANT)
            {
                PotVegetable potVegetable = _uow.PotVegetable.GetUnharvestPotVegetable(id);
                if (potVegetable != null)
                {
                    pot.Status = PotStatus.BLANK;
                    potVegetable.Status = PotVegetableStatus.HARVESTED;
                    potVegetable.DateHarvest = DateTime.Now;

                    _uow.Commit();
                    return true;
                }
            }

            return false;
        }
        #endregion
    }
}
