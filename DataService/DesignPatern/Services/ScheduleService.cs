﻿using DataService.DesignPatern.Handler;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Security;
using DataService.DesignPatern.ServiceModels;
using DataService.DesignPatern.UnitOfWorks;
using DataService.Models;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.Services
{
    public interface IScheduleService
    {
        ScheduleServiceModel CreateNewSchedule(CreateScheduleRequestModel model);
        ScheduleHandlerServiceModel GetScheduleByID(int id);
        ScheduleServiceModel UpdateSchedule(int id, UpdateScheduleRequestModel model);
        bool DeleteSchedule(int id, bool mode);
        IEnumerable<Schedule> GetSchedules();
        bool StartOrStopSchedule(int id, bool mode);
    }
    public class ScheduleService : BaseUnitOfWork<UnitOfWork>, IScheduleService
    {
        private readonly AppSettings _appSettings;
        public VegetableService _service;
        public CallScriptPythonService _callService;
        public NotificationFireBaseService _serviceNoti;
        public ScheduleService(UnitOfWork uow, IOptions<AppSettings> appSettings,VegetableService service, CallScriptPythonService callService, NotificationFireBaseService serviceNoti) : base(uow)
        {
            _appSettings = appSettings.Value;
            _service = service;
            _callService = callService;
            _serviceNoti = serviceNoti;




        }

        #region Get list schedules
        public IEnumerable<Schedule> GetSchedules()
        {
            var schedules = _uow.Schedule.GetSchedules();

            if (schedules != null && schedules.Any())
            {
                return schedules;
            }

            return null;
        }
        #endregion

        #region Create new Schedule
        public ScheduleServiceModel CreateNewSchedule(CreateScheduleRequestModel model)
        {
            var userUnexist = _uow.User.CheckUsername(model.Username);

            if (userUnexist)
            {
                return null;
            } else
            {
                var schedule = new Schedule
                {
                    Username = model.Username,
                    Time = model.Time,
                    IsDelete = model.IsDelete
                };

                _uow.Schedule.Create(schedule);
                _uow.Commit();

                return new ScheduleServiceModel
                {
                    ID = schedule.Id,
                    Username = model.Username,
                    Time = model.Time,
                    IsDelete = model.IsDelete
                };
            }
        }
        #endregion

        #region Get schedule by id
        public ScheduleHandlerServiceModel GetScheduleByID(int id)
        {
            var schedule = _uow.Schedule.GetById(id);

            if (schedule != null)
            {
                var jobName = EncryptorService.MD5Hash(schedule.Username + "_" + schedule.Id);
                var handler = new HandlerJob { JobName = jobName };
                return new ScheduleHandlerServiceModel
                {
                    ID = schedule.Id,
                    Username = schedule.Username,
                    Time = schedule.Time,
                    IsDelete = schedule.IsDelete,
                    IsRunning = handler.AnyJob()
                };
            }

            return null;
        }
        #endregion

        #region Update schedule
        public ScheduleServiceModel UpdateSchedule(int id, UpdateScheduleRequestModel model)
        {
            var schedule = _uow.Schedule.GetById(id);

            if(schedule != null)
            {
                schedule.Time = model.Time == null ? schedule.Time : model.Time;
                schedule.IsDelete = model.IsDelete;

                _uow.Commit();

                StartOrStopSchedule(schedule.Id, true);

                return new ScheduleServiceModel
                {
                    ID = schedule.Id,
                    Username = schedule.Username,
                    Time = schedule.Time,
                    IsDelete = schedule.IsDelete
                };
            }

            return null;
        }
        #endregion

        #region Delete schedule
        public bool DeleteSchedule(int id, bool mode)
        {
            var schedule = _uow.Schedule.GetById(id);

            if (schedule != null)
            {
                schedule.IsDelete = mode;

                _uow.Commit();

                return true;
            }

            return false;
        }
        #endregion

        #region Start of Stop schedule mode(true: start - false: stop)
        public bool StartOrStopSchedule(int id, bool mode)
        {
            var schedule = _uow.Schedule.GetById(id);

            if (schedule != null)
            {
                var jobName = EncryptorService.MD5Hash(schedule.Username + "_" + schedule.Id);
                var handlerJob = new HandlerJob
                {
                    Time = schedule.Time,
                    JobName = jobName,
                    Username = schedule.Username,
                    _service = _service,
                    _callService = _callService,
                    _uow = _uow,
                    _serviceNoti = _serviceNoti
                };

                if (mode)
                {
                    handlerJob.StartJob();
                    return handlerJob.AnyJob();
                } else
                {
                    handlerJob.StopJob();
                    return !handlerJob.AnyJob();
                }
            }

            return false;
        }
        #endregion
    }
}
