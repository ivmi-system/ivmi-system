﻿using DataService.DesignPatern.Enum;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Security;
using DataService.DesignPatern.UnitOfWorks;
using DataService.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Services
{
    public interface IRequestService
    {
        IEnumerable<Request> GetAllRequests(int filter = 0, int size = 10, int page = 1);
        Request Create(CreateRequestRequestModel model);
        Request UpdateStatus(int requestId, int status = 1);
    }
    public class RequestService : BaseUnitOfWork<UnitOfWork>, IRequestService
    {
        private readonly AppSettings _appSettings;
        public RequestService(UnitOfWork uow, IOptions<AppSettings> appSettings) : base(uow)
        {
            _appSettings = appSettings.Value;

        }

        public IEnumerable<Request> GetAllRequests(int filter = 0, int size = 10, int page = 1)
        {
            var requests = _uow.Request.GetAllRequests(filter, size, page);

            if (requests != null && requests.Any())
            {
                return requests;
            }

            return null;
        }

        public Request Create(CreateRequestRequestModel model)
        {
            var request = new Request
            {
                Name = model.Name,
                Address = model.Address,
                Email = model.Email,
                Phone = model.Phone,
                Budget = model.Budget,
                CardNumber = model.CardNumber,
                Status = RequestStatus.PENDING,
                IsDelete = false
            };

            _uow.Request.Create(request);
            _uow.Commit();

            return request.Id > 0 ? request : null;
        }

        public Request UpdateStatus(int requestId, int status = 1)
        {
            var request = _uow.Request.GetById(requestId);

            if (request != null && request.IsDelete == false)
            {
                request.Status = status;

                _uow.Commit();
                return request;
            }

            return null;
        }
    }
}
