﻿using DataService.DesignPatern.Enum;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Security;
using DataService.DesignPatern.ServiceModels;
using DataService.DesignPatern.UnitOfWorks;
using DataService.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.Services
{
    public interface IPotVegetableService
    {
        IEnumerable<PotVegetable> GetListPotVegetable(int size = 10, int page = 1);
        PotVegetableServiceModel CreateNewPotVegetable(CreatePotVegetableRequestModel model);
        PotVegetableServiceModel UpdatePotVegetable(UpdatePotVegetableRequestModel model);
        bool DeletePotVegetable(int id, bool mode);
    }

    public class PotVegetableService : BaseUnitOfWork<UnitOfWork>, IPotVegetableService
    {
        private readonly AppSettings _PotVegetableSettings;

        public PotVegetableService(UnitOfWork uow, IOptions<AppSettings> appSettings) : base(uow)
        {
            _PotVegetableSettings = appSettings.Value;
        }

        #region Get list potVegetable
        public IEnumerable<PotVegetable> GetListPotVegetable(int size = 10, int page = 1)
        {
            IEnumerable<PotVegetable> potvegetable = _uow.PotVegetable.GetListPotVegetable(size, page);

            if (potvegetable != null)
            {
                return potvegetable;
            }

            return null;
        }
        #endregion

        #region Create new potVegetable
        public PotVegetableServiceModel CreateNewPotVegetable(CreatePotVegetableRequestModel model)
        {
            Pot pot = _uow.Pot.GetById(model.PotID);
            Vegetable vegetable = _uow.Vegetable.GetById(model.VegetableID);

            if (pot != null && pot.IsDelete == false && pot.Status != PotStatus.PLANT && vegetable != null && vegetable.IsDelete == false)
            {
                PotVegetable potVegetable = new PotVegetable
                {
                    PotId = model.PotID,
                    VegetableId = model.VegetableID,
                    DatePlant = DateTime.Now,
                    Status = PotVegetableStatus.PLANTING,
                    IsDelete = false
                };

                pot.Status = PotStatus.PLANT;

                _uow.PotVegetable.Create(potVegetable);
                _uow.Commit();

                return new PotVegetableServiceModel
                {
                    ID = potVegetable.Id,
                    PotID = potVegetable.PotId,
                    VegetableId = potVegetable.VegetableId,
                    DatePlant = potVegetable.DatePlant,
                    DateHarvest = potVegetable.DateHarvest,
                    Status = potVegetable.Status,
                    UrlImage = potVegetable.UrlImage,
                    IsDelete = potVegetable.IsDelete
                };
            }

            return null;
        }
        #endregion

        #region Update potVegetable
        public PotVegetableServiceModel UpdatePotVegetable(UpdatePotVegetableRequestModel model)
        {
            var potvegetable = _uow.PotVegetable.GetPotVegetableById(model.PotID);

            if (potvegetable != null)
            {
                var potvegetables = new PotVegetable
                {
                    PotId = model.PotID,
                    VegetableId = model.VegetableID,
                    DatePlant = model.DatePlant,
                    DateHarvest = model.DateHarvest,
                    Status = model.Status,
                    UrlImage = model.UrlImage,
                    IsDelete = model.IsDelete
                };
                _uow.Commit();
                return new PotVegetableServiceModel
                {
                    ID = potvegetable.Id,
                    PotID = potvegetables.PotId,
                    VegetableId = potvegetables.VegetableId,
                    DatePlant = potvegetables.DatePlant,
                    DateHarvest = potvegetables.DateHarvest,
                    Status = potvegetables.Status,
                    UrlImage = potvegetables.UrlImage,
                    IsDelete = potvegetables.IsDelete
                };
            }
            return null;
        }
        #endregion

        #region Delete potVegetable
        public bool DeletePotVegetable(int id, bool mode)
        {
            var potvegetable = _uow.PotVegetable.GetPotVegetableById(id);

            if (potvegetable != null)
            {
                potvegetable.IsDelete = mode;

                _uow.Commit();

                return true;
            }

            return false;
        }
        #endregion

    }
}
