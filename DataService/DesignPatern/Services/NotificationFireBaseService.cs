﻿using DataService.DesignPatern.UnitOfWorks;
using FirebaseAdmin.Messaging;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.Services
{
    public class NotificationFireBaseService : BaseUnitOfWork<UnitOfWork>
    {
        public NotificationFireBaseService(UnitOfWork uow) : base(uow)
        {
        }
        #region send notification
        public bool SendNotification(string username, int disease)
        {
            // get device token by username
            string deviceToken = _uow.User.GetDeviceToken(username);
            //send notification to mobile

           string contentBody = "";
            if (disease == 0)
            {
                contentBody = "Luống rau của bạn đang phát triển tốt";
            }
            if (disease == 1)
            {
                contentBody = "Luống rau của bạn hiện đang có vấn đề";
            }

            Notification noti = new Notification
            {
                Title = "Thông báo mới về vườn rau của bạn",
                Body = contentBody,
                ImageUrl = "https://scontent-hkg4-1.xx.fbcdn.net/v/t1.15752-9/104690973_1131015940618315_5001668473883476875_n.png?_nc_cat=103&_nc_sid=b96e70&_nc_ohc=qWnAj8TVmPsAX9HVLsG&_nc_ht=scontent-hkg4-1.xx&oh=0109766658c55a842872a3209512418f&oe=5F1948BF"
            };
            Dictionary<string, string> data = new Dictionary<string, string> {
                    { "test","test"}
                };
            bool check = FireBaseClients.FirebaseClient.Send(deviceToken, noti, data);
            if (check == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}
