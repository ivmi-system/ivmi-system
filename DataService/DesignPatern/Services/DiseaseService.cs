﻿using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Security;
using DataService.DesignPatern.ServiceModels;
using DataService.DesignPatern.UnitOfWorks;
using DataService.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.Services
{
    public interface IDiseaseService
    {
        DiseaseServiceModel GetDiseaseID(int id);
        DiseaseServiceModel UpdateDisease(int id, UpdateDiseaseRequestModel model);
        bool DeleteDisease(int id, bool mode);
        DiseaseServiceModel CreateNewDisease(CreateDiseaseRequestModel model);
    }
    public class DiseaseService : BaseUnitOfWork<UnitOfWork>, IDiseaseService
    {
       
        public DiseaseService(UnitOfWork uow) : base(uow)
        {
            
        }

        #region Get disease by ID
        public DiseaseServiceModel GetDiseaseID(int id)
        {
            var disease = _uow.Disease.GetById(id);

            if (disease != null)
            {
                return new DiseaseServiceModel
                {
                    ID = disease.Id,
                    Name = disease.Name,
                    LabelID = disease.LabelId,
                    Description = disease.Description,
                    IsDelete = disease.IsDelete                   
                    
                };
            }
            return null;
        }
        #endregion

        #region Update Disease
        public DiseaseServiceModel UpdateDisease(int id, UpdateDiseaseRequestModel model)
        {
            var disease = _uow.Disease.GetById(id);

            if (disease != null)
            {
                disease.Name = model.Name;
                disease.LabelId = model.LabelID;
                disease.Description = model.Description;
                disease.IsDelete = model.IsDelete;
                _uow.Disease.Update(disease);
                _uow.Commit();

                return new DiseaseServiceModel
                {
                    ID = disease.Id,
                    Name = disease.Name,
                    LabelID = disease.LabelId,
                    Description = disease.Description,
                    IsDelete = disease.IsDelete

                };
            }
            return null;
        }
        #endregion

        #region Delete Disease
        public bool DeleteDisease(int id, bool mode)
        {
            var disease = _uow.Disease.GetById(id);

            if (disease != null)
            {
                disease.IsDelete = mode;

                _uow.Commit();

                return true;
            }

            return false;
        }
        #endregion

        #region Create new Disease
        public DiseaseServiceModel CreateNewDisease(CreateDiseaseRequestModel model)
        {
            var userUnexist = _uow.User.CheckUsername(model.Name);

            if (userUnexist)
            {
                return null;
            }
            else
            {
                var disease = new Disease
                {
                    Name = model.Name,
                    LabelId = model.LabelID,
                    Description = model.Description,
                    IsDelete = model.IsDelete
                };

                
                _uow.Commit();

                return new DiseaseServiceModel
                {
                    ID = disease.Id,
                    Name = disease.Name,
                    LabelID = disease.LabelId,
                    Description = disease.Description,
                    IsDelete = disease.IsDelete
                };
            }
        }
        #endregion
    }

}
