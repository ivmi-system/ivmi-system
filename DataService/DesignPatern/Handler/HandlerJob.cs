﻿using DataService.DesignPatern.Enum;
using DataService.DesignPatern.Services;
using DataService.DesignPatern.UnitOfWorks;
using DataService.Models;
using DataService.Models.ModelCreate;
using FluentScheduler;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DesignPatern.Handler
{
    public class HandlerJob : Registry
    {
        public string JobName { get; set; }
        public string Username { get; set; }
        public TimeSpan? Time { get; set; }
        
        public  VegetableService _service;
        public CallScriptPythonService _callService;
        public UnitOfWork _uow;
        public NotificationFireBaseService _serviceNoti;

        public HandlerJob(TimeSpan dateTime, string jobName,string username, VegetableService service,CallScriptPythonService callService, UnitOfWork uow, NotificationFireBaseService notiService,IServiceProvider provider)
        {
            this.Time = dateTime;
            this.JobName = jobName;
            this.Username = username;
            _service = service;
            _callService = callService;
            CapstoneDbContext dbContext = provider.GetRequiredService<CapstoneDbContext>();
            _uow = provider.GetRequiredService<UnitOfWork>();
            _serviceNoti = notiService;
        }
        public HandlerJob()
        {

        }
        /**
         * Return true if this job name has already in Schedules
         * 
         */
        public bool AnyJob()
        {
            return JobManager.AllSchedules.Any(x => x.Name == this.JobName);
        }

        public void StartJob()
        {
            this.StopJob();

            // var schedule = this.Schedule(() => null);

         

            Schedule(() =>this.TaskAsync()).WithName(this.JobName).ToRunEvery(1).Days().At(this.GetHour(), this.GetMinute());


            // schedule.WithName(this.JobName).ToRunEvery(1).Days().At(this.GetHour(), this.GetMinute());

            this.Initialize();
            // System.Diagnostics.Debug.WriteLine("======== Schedule: " + JobName + " is running");
        }

        private int GetHour()
        {
            return Int32.Parse(DateTime.Parse(this.Time.ToString()).ToString("HH"));
        }

        private int GetMinute()
        {
            return Int32.Parse(DateTime.Parse(this.Time.ToString()).ToString("mm"));
        }

        public void StopJob()
        {
            JobManager.RemoveJob(this.JobName);
        }

        public void StopJob(string name)
        {
            JobManager.RemoveJob(name);
        }
        private int pvdStatus(PotVegetableDisease pvd, int status)
        {
            return pvd != null && pvd.Status == status ? 1 : 0;
        }

        private PotVegetableDisease createPotVegetableDisease(int potVegetableId, int diseaseId)
        {
            PotVegetableDisease potVegetableDisease = new PotVegetableDisease
            {
                PotVegetableId = potVegetableId,
                DiseaseId = diseaseId,
                DateBegin = DateTime.Now,
                Status = PotVegetableDiseaseStatus.UN_RESOLVE
            };
            _uow.PotVegetableDisease.Create(potVegetableDisease);

            return potVegetableDisease;
        }
        public async Task<bool> DetectAndSendNoti(string username)
        {
            
            var listVegetable = _callService.CallScriptPython();
            if (listVegetable != null)
            {

                foreach (VegetableModel detectionVegetable in listVegetable)
                {
                    //Pot pot = _uow.Pot.GetPotWithUsernameAndPosition(username,detectionVegetable.Pot);
                    Pot pot = await _uow.Pot.Get().Where(p => p.Position == detectionVegetable.Pot).FirstOrDefaultAsync();

                    PotVegetable potVegetable = await _uow.PotVegetable.GetPotVegetableWithPotIDAsync(pot.Id);

                    // neu pot vegetable == null
                    // thi o vi tri nay chua co cay nao duoc trong
                    if (potVegetable != null)
                    {
                        // notification
                        // Notification noti = createNotification(username, pot.PlotId);

                        // Find disease with labelId
                        foreach (int labelId in detectionVegetable.listDisease)
                        {
                            Disease disease = _uow.Disease.GetByLabelId(labelId);

                            // Find lastest PotVegetableDisease
                            PotVegetableDisease pvdLastest = _uow.PotVegetableDisease.GetLastest(potVegetable.Id, disease.Id);

                            int flag = 0; // 0 is create new
                            flag += pvdStatus(pvdLastest, PotVegetableDiseaseStatus.UN_RESOLVE);
                            if (pvdLastest != null && pvdLastest.Status == PotVegetableDiseaseStatus.DELAY)
                            {
                                var plant = potVegetable.DatePlant;
                                var now = DateTime.Now;
                                flag++;
                            }


                            if (flag == 0)
                            {
                                PotVegetableDisease pvd = createPotVegetableDisease(potVegetable.Id, disease.Id);
                            }
                        }

                        // Con sau
                        if (detectionVegetable.ConSau > 0) // > 0 co sau
                        {
                            potVegetable.HasWorm = true;

                        }
                        else
                        {
                            potVegetable.HasWorm = false;
                        }
                        // Chau cay bi trong
                        if (detectionVegetable.Blank > 0)
                        {
                            // pot.Status = PotStatus.EMPTY;
                        }

                        potVegetable.UrlImage = detectionVegetable.UrlImage;

                    }
                }

                _uow.Commit();
                _serviceNoti.SendNotification(username, 1);
                return true;
            }
            else
            {
                return false;
            }


        }
        public async Task TaskAsync()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44364/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                string url = "api/vegetables/detect-and-send-notification";
                HttpResponseMessage responseMessage = await client.GetAsync(url);
                string json = await responseMessage.Content.ReadAsStringAsync();
                Console.WriteLine(json);
            }
        }

        public void Initialize()
        {
            JobManager.Initialize(this);
        }
    }
}
