﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Repositories.Basic
{
   public interface IBaseRepository<TEntity> where TEntity : class
    {
        TEntity GetById(int id);
        IQueryable<TEntity> Get();
        TEntity Create(TEntity entity);
        TEntity Update(TEntity entity);
        TEntity Remove(int entity);
        TEntity FirstOrDefault();
    }
}
