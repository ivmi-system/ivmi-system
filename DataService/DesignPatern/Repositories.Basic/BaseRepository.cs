﻿using DataService.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Repositories.Basic
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private readonly CapstoneDbContext context;
        private readonly DbSet<TEntity> dbSet;
        public BaseRepository(CapstoneDbContext Context)
        {
            context = Context;
            this.dbSet = context.Set<TEntity>();
        }
        public TEntity Create(TEntity entity)
        {
            return dbSet.Add(entity).Entity;
        }

        public TEntity FirstOrDefault()
        {
            return dbSet.FirstOrDefault();
        }

        public IQueryable<TEntity> Get()
        {
            return dbSet;
        }

        public TEntity GetById(int id)
        {
            return dbSet.Find(id);
        }

        public TEntity Remove(int entity)
        {

            var key = dbSet.Find(entity);
            if (key != null)
            {
                return dbSet.Remove(key).Entity;
            }
            else
            {
                return null;
            }
        }

        public TEntity Update(TEntity entity)
        {
            var entry = context.Entry(entity);
            entry.State = EntityState.Modified;
            return entry.Entity;
        }
    }
}
