﻿using DataService.DesignPatern.Repositories.Create;
using DataService.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CapstoneDbContext _dbContext;
        //
        //delacre Repository
        public UserRepository User { get; private set; }
        public VegetableRepository Vegetable { get; private set; }
        public ScheduleRepository Schedule { get; private set; }
        public PlotRepository Plot { get; private set; }
        public PotRepository Pot { get; private set; }
        public NotificationRepository Notification { get; private set; }
        public ContractRepository Contract { get; private set; }
        public DiseaseRepository Disease { get; private set; }
        public PotVegetableRepository PotVegetable { get; private set; }
        public TempRepository Temp { get; private set; }
        public PotVegetableDiseaseRepository PotVegetableDisease { get; private set; }
        public NotificationPotVegetableDiseaseRepository NotificationPotVegetableDisease { get; set; }
        public ImageDiseaseRepository ImageDisease { get; set; }
        public RequestRepository Request { get; set; }

        public UnitOfWork(CapstoneDbContext dbContext)
        {
            this._dbContext = dbContext;
            this.User = new UserRepository(this._dbContext);
            this.Vegetable = new VegetableRepository(this._dbContext);
            this.Schedule = new ScheduleRepository(this._dbContext);
            this.Plot = new PlotRepository(this._dbContext);
            this.Pot = new PotRepository(this._dbContext);
            this.Notification = new NotificationRepository(this._dbContext);
            this.Contract = new ContractRepository(this._dbContext);
            this.Disease = new DiseaseRepository(this._dbContext);
            this.PotVegetable = new PotVegetableRepository(this._dbContext);
            this.Temp = new TempRepository(this._dbContext);
            this.PotVegetableDisease = new PotVegetableDiseaseRepository(this._dbContext);
            this.NotificationPotVegetableDisease = new NotificationPotVegetableDiseaseRepository(this._dbContext);
            this.ImageDisease = new ImageDiseaseRepository(this._dbContext);
            this.Request = new RequestRepository(this._dbContext);
        }

        public void Commit()
        {
            this._dbContext.SaveChanges();
        }

        public void Dispose()
        {
            this._dbContext.Dispose();
        }
    }
}
