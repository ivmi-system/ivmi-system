﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.UnitOfWorks
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
    }
}
