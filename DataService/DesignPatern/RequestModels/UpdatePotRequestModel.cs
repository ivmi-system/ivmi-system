﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.RequestModels
{
    public class UpdatePotRequestModel
    {
        public int Position { get; set; }
        public int Status { get; set; }
        public string Coordinates { get; set; }
    }
}
