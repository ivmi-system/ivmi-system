﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.RequestModels
{
    public class UpdateContractRequestModel
    {
        public DateTime DateCreate { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime Duration { get; set; }
        public float Fee { get; set; }
        public bool IsDelete { get; set; }
    }
}
