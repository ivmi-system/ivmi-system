﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.RequestModels
{
    public class UpdateDeleteNotificationRequestModel
    {
        public string UserName { get; set; }
        public int PlotID { get; set; }
        public bool IsDelete { get; set; }
    }
}
