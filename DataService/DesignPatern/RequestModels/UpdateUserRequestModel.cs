﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.RequestModels
{
    public class UpdateUserRequestModel
    {
        public string Password { get; set; }
        public string Name { get; set; }
        public string DeviceToken { get; set; }
        public string Phone { get; set; }
        public bool IsDelete { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string CameraIp { get; set; }
    }
}
