﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.RequestModels
{
    public class CreateImageDiseaseRequestModel
    {
        public int VegetableId { get; set; }
        public string Path { get; set; }
        public string FeedbackContent { get; set; }
    }
}
