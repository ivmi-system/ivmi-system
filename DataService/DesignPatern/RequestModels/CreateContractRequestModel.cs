﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.RequestModels
{
    public class CreateContractRequestModel
    {
        public string Username { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime Duration { get; set; }
        public float Fee { get; set; }
    }
}
