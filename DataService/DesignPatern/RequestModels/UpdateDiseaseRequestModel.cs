﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.RequestModels
{
    public class UpdateDiseaseRequestModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int LabelID { get; set; }
        public string Description { get; set; }
        public bool IsDelete { get; set; }

    }
}
