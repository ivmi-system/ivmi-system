﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.RequestModels
{
    public class UpdateSeenNotificationRequestModel
    {
        public string UserName { get; set; }
        public int PlotID { get; set; }
        public bool IsSeen { get; set; }
    }
}
