﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.RequestModels
{
    public class UpdateScheduleRequestModel
    {
        public TimeSpan Time { get; set; }
        public bool IsDelete { get; set; }
    }
}
