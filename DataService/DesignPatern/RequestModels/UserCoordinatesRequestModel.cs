﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.RequestModels
{
    public class UserCoordinatesRequestModel
    {
        public int id { get; set; }
        public string coordinates { get; set; }
    }
}
