﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.RequestModels
{
    public class UpdatePotVegetableRequestModel
    {
        public int PotID { get; set; }
        public int VegetableID { get; set; }
        public DateTime DatePlant { get; set; }
        public DateTime DateHarvest { get; set; }
        public int Status { get; set; }
        public string UrlImage { get; set; }
        public bool IsDelete { get; set; }
    }
}
