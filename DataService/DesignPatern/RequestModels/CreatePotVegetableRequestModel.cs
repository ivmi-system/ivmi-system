﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.RequestModels
{
    public class CreatePotVegetableRequestModel
    {
        public int PotID { get; set; }
        public int VegetableID { get; set; }
    }
}
