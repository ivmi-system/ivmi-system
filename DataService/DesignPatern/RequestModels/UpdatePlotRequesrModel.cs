﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.RequestModels
{
    public class UpdatePlotRequesrModel
    {
        public string Line { get; set; }
        public int Size { get; set; }
        public bool IsDelete { get; set; }
    }
}
