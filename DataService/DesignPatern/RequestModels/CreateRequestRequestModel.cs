﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.RequestModels
{
    public class CreateRequestRequestModel
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int Budget { get; set; }
        public long CardNumber { get; set; }
    }
}
