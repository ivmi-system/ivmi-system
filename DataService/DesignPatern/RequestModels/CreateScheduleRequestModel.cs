﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.RequestModels
{
    public class CreateScheduleRequestModel
    {
        public string Username { get; set; }
        public TimeSpan Time { get; set; }
        public bool IsDelete { get; set; }
    }
}
