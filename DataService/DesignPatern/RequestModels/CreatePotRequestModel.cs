﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.RequestModels
{
    public class CreatePotRequestModel
    {
        public int PlotID { get; set; }
        public int Position { get; set; }
        public int Status { get; set; }
        public bool IsDelete { get; set; }
    }
}
