﻿using DataService.DesignPatern.Repositories.Basic;
using DataService.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.Repositories.Create
{
    public interface INotificationPotVegetableDiseaseRepository : IBaseRepository<NotificationPotVegetableDisease>
    {

    }
    public class NotificationPotVegetableDiseaseRepository : BaseRepository<NotificationPotVegetableDisease>, INotificationPotVegetableDiseaseRepository
    {
        public NotificationPotVegetableDiseaseRepository(CapstoneDbContext Context) : base(Context) { }
    }
}
