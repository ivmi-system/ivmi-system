﻿using DataService.DesignPatern.Repositories.Basic;
using DataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Repositories.Create
{
    public interface IPlotRepository : IBaseRepository<Plot>
    {
        Plot GetUserPlotByID(string username, int id);
        IEnumerable<Plot> GetUserPlots(string username);
    }

    public class PlotRepository : BaseRepository<Plot>, IPlotRepository
    {
        public PlotRepository(CapstoneDbContext context) : base(context) { }

        public Plot GetUserPlotByID(string username, int id) => Get().Where(p => p.Username == username && p.Id == id).FirstOrDefault();

        public IEnumerable<Plot> GetUserPlots(string username) => Get().Where(p => p.Username == username && p.IsDelete == false);
    }
}
