﻿using DataService.DesignPatern.Repositories.Basic;
using DataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Repositories.Create
{
    public interface INotificationRepository : IBaseRepository<Notification>
    {
        IEnumerable<Notification> GetUserNotifications(string username, int size = 10, int page = 1);
        IEnumerable<Notification> GetUserNotifications(string username, bool isSeen, int size = 10, int page = 1);
        IEnumerable<Notification> GetPlotNotifications(int id, int size = 10, int page = 1);
        Notification GetByID(int id);
        IEnumerable<Notification> GetListNotifications(int size = 10, int page = 1);
        IEnumerable<Notification> GetUserUnreadNotification(string username);
    }
    public class NotificationRepository : BaseRepository<Notification>, INotificationRepository
    {
        public NotificationRepository(CapstoneDbContext context) : base(context) { }

        public IEnumerable<Notification> GetUserNotifications(string username, int size = 10, int page = 1)
        {
            return Get().Where(p => p.Username == username && p.IsDelete == false)
                   .Skip((page - 1) * size)
                   .Take(size).ToList();
        }

        public IEnumerable<Notification> GetUserNotifications(string username, bool isSeen, int size = 10, int page = 1)
        {
            return Get().Where(p => p.Username == username && p.IsSeen == isSeen && p.IsDelete == false)
                   .Skip((page - 1) * size)
                   .Take(size).ToList();
        }

        public IEnumerable<Notification> GetPlotNotifications(int id,int size = 10, int page = 1)
        {
            return Get().Where(p => p.PlotId == id && p.IsDelete == false)
                   .Skip((page - 1) * size)
                   .Take(size).ToList();
        }

        public Notification GetByID(int id) => Get().Where(p => p.Id == id).FirstOrDefault();

        public IEnumerable<Notification> GetListNotifications(int size = 10, int page = 1)
        {
            return Get().Where(p => p.IsDelete == false).Skip((page - 1) * size).Take(size).ToList();
        }

        public IEnumerable<Notification> GetUserUnreadNotification(string username)
        {
            return Get().Where(p => p.Username == username && p.IsDelete == false && p.IsSeen == false);
        }
    }
}
