﻿using DataService.DesignPatern.Enum;
using DataService.DesignPatern.Repositories.Basic;
using DataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Repositories.Create
{
    public interface IPotVegetableDiseaseRepository : IBaseRepository<PotVegetableDisease>
    {
        PotVegetableDisease GetLastest(int potVegetableId, int diseaseId);
    }
    public class PotVegetableDiseaseRepository : BaseRepository<PotVegetableDisease>, IPotVegetableDiseaseRepository
    {
        public PotVegetableDiseaseRepository(CapstoneDbContext context) : base(context)
        {

        }

        public PotVegetableDisease GetLastest(int potVegetableId, int diseaseId)
        {
            return Get()
                    .Where(p => p.PotVegetableId == potVegetableId && p.DiseaseId == diseaseId && p.IsDelete == false)
                    .OrderByDescending(p => p.DateBegin)
                    .FirstOrDefault();
        }
    }

}
