﻿using DataService.DesignPatern.Repositories.Basic;
using DataService.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.Repositories.Create
{
    public interface ITempRepository : IBaseRepository<Temp>
    {
     
    }
    public class TempRepository : BaseRepository<Temp>, ITempRepository
    {
        public TempRepository(CapstoneDbContext Context) : base(Context)
        {
        }
    }
}
