﻿using DataService.DesignPatern.Enum;
using DataService.DesignPatern.Repositories.Basic;
using DataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Repositories.Create
{
    public interface IContractRepository : IBaseRepository<Contract>
    {
        IEnumerable<Contract> GetContracts(int size = 10, int page = 1);
        IEnumerable<Contract> GetUserContracts(string username, int size = 10, int page = 1, bool lastest = false);
        Contract GetContractByID(int id);
        Contract GetLastestDurationUser(string username);
        bool IsAvailableNewContract(string username);
    }
    public class ContractRepository : BaseRepository<Contract>, IContractRepository
    {
        public ContractRepository(CapstoneDbContext context) : base(context) { }
        public IEnumerable<Contract> GetContracts(int size = 10, int page = 1)
        {
            return Get().Where(p => p.IsDelete == false).Skip((page - 1) * size).Take(size).ToList();
        }
        public IEnumerable<Contract> GetUserContracts(string username, int size = 10, int page = 1, bool lastest = false)
        {
            if (lastest)
            {
                var result = Get().Where(p => p.Username == username && p.IsDelete == false);
                return result.OrderBy(p => p.DateCreate).OrderByDescending(p=>p.DateCreate).Take(1).ToList();
            }
            return Get().Where(p => p.Username == username && p.IsDelete == false).OrderByDescending(p => p.DateCreate).Skip((page - 1) * size).Take(size).ToList();
        }

        public Contract GetContractByID(int id) => Get().Where(p => p.Id == id).FirstOrDefault();
        public Contract GetLastestDurationUser(string username)
        {
            return Get().Where(p => p.Username == username && p.IsDelete == false).OrderByDescending(p => p.Duration).FirstOrDefault();
        }
        public bool IsAvailableNewContract(string username)
        {
            var contract = GetLastestDurationUser(username);

            if (contract == null)
            {
                return true;
            } else if (contract != null && contract.Status == ContractStatus.PAUSED)
            {
                return true;
            }

            return false;
        }
    }
}
