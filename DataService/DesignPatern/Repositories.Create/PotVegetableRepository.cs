﻿using DataService.DesignPatern.Enum;
using DataService.DesignPatern.Repositories.Basic;
using DataService.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataService.DesignPatern.Repositories.Create
{
    public interface IPotVegetableRepository : IBaseRepository<PotVegetable>
    {
        PotVegetable GetPotVegetableById(int id);        
        IEnumerable<PotVegetable> GetListPotVegetable(int size = 10, int page = 1);
        PotVegetable GetPotVegetableWithPotID(int potId);
        PotVegetable GetUnharvestPotVegetable(int potId);
    }
    public class PotVegetableRepository : BaseRepository<PotVegetable>, IPotVegetableRepository
    {
        public PotVegetableRepository(CapstoneDbContext context) : base(context) { }
        public PotVegetable GetPotVegetableById(int id) => Get().Where(p => p.Id == id && p.IsDelete == false).FirstOrDefault();       

        public IEnumerable<PotVegetable> GetListPotVegetable(int size = 10, int page = 1)
        {
            return Get().Where(p => p.IsDelete == false).Skip((page - 1) * size).Take(size).ToList();
        }

        public PotVegetable GetPotVegetableWithPotID(int potId)
        {
            return Get().Where(p => p.PotId == potId && p.DateHarvest == null && p.IsDelete == false).FirstOrDefault();
        }
        public async Task<PotVegetable> GetPotVegetableWithPotIDAsync(int potId)
        {
            return await Get().Where(p => p.PotId == potId && p.DateHarvest == null && p.IsDelete == false).FirstOrDefaultAsync();
        }

        public PotVegetable GetUnharvestPotVegetable(int potId)
        {
            return Get().Where(p => p.PotId == potId && p.Status != PotVegetableStatus.HARVESTED && p.IsDelete == false).FirstOrDefault();
        }
    }
}
