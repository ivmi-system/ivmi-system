﻿using DataService.DesignPatern.Repositories.Basic;
using DataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Repositories.Create
{

    public interface IVegetableRepository : IBaseRepository<Vegetable>
    {
        Vegetable GetVegetableById(int id);
        Vegetable GetVegetableByLableId(int labelId);
        IEnumerable<Vegetable> GetListVegetables(int size = 10, int page = 1);
    }
    public class VegetableRepository : BaseRepository<Vegetable>, IVegetableRepository
    {
        public VegetableRepository(CapstoneDbContext Context) : base(Context)
        {
        }

        public Vegetable GetVegetableById(int id)
        {
            return Get().Where(p => p.Id == id && p.IsDelete == false).FirstOrDefault();
        }

        public Vegetable GetVegetableByLableId(int labelId)
        {
            return Get().Where(p => p.LabelId == labelId).FirstOrDefault();
        }

        public IEnumerable<Vegetable> GetListVegetables(int size = 10, int page = 1)
        {
            return Get().Where(p => p.IsDelete == false).Skip((page - 1) * size).Take(size).ToList();
        }
    }
}
