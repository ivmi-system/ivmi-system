﻿using DataService.DesignPatern.Repositories.Basic;
using DataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Repositories.Create
{
    public interface IImageDiseaseRepository : IBaseRepository<ImageDisease>
    {
        IEnumerable<ImageDisease> GetAll(int size = 10, int page = 1);
    }
    public class ImageDiseaseRepository : BaseRepository<ImageDisease>, IImageDiseaseRepository
    {
        public ImageDiseaseRepository(CapstoneDbContext context) : base(context) { }

        public IEnumerable<ImageDisease> GetAll(int size = 10, int page = 1)
        {
            return Get().Where(p => p.IsDelete == false).Skip((page - 1) * size).Take(size).ToList();
        }
    }
}
