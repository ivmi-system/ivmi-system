﻿using DataService.DesignPatern.Repositories.Basic;
using DataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Repositories.Create
{
    public interface IUserRepository : IBaseRepository<User>
    {
        User CheckLogin(string username, string password);
        IEnumerable<User> GetAllUser(string currentUsername);
        IEnumerable<User> GetAllUser(string currentUsername, int size = 10, int page = 1);
        bool CheckUsername(string username);
        string GetDeviceToken(string username);
        User GetUserByUsername(string username, bool isDelete);
        User GetUserByUsername(string username);
    }
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(CapstoneDbContext Context) : base(Context)
        {
        }

        public User CheckLogin(string username, string password)
        {
            return Get().Where(p => p.Username == username && p.Password == password && p.IsDelete == false).FirstOrDefault();
        }

        /**
         * 
         * Return true if user is not exist in database and false if user has already.
         * 
         */
        public bool CheckUsername(string username)
        {
            var check = Get().Where(p => p.Username == username).FirstOrDefault();
            if (check != null)
            {
                return false;
            }
            return true;
        }

        public IEnumerable<User> GetAllUser(string currentUsername)
        {
            return Get().Where(p => p.IsDelete == false && p.Username != currentUsername);
        }

        public IEnumerable<User> GetAllUser(string currentUsername,int size, int page)
        {
            page = page == 0 ? 1 : page;
            size = size == 0 ? 10 : size;
            var skip = (page - 1) * size;
            return Get().Where(p => p.IsDelete == false && p.Username != currentUsername).Skip(skip).Take(size).ToList();
        }

        public string GetDeviceToken(string username)
        {
            return Get().Where(p => p.Username == username && p.IsDelete == false).Select(u => u.DeviceToken).FirstOrDefault();
        }

        public User GetUserByUsername(string username, bool isDelete)
        {
            return Get().Where(p => p.Username == username && p.IsDelete == isDelete).FirstOrDefault();
        }

        public User GetUserByUsername(string username)
        {
            return Get().Where(p => p.Username == username).FirstOrDefault();
        }
    }
}
