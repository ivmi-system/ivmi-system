﻿using DataService.DesignPatern.Repositories.Basic;
using DataService.DesignPatern.ServiceModels;
using DataService.Models;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Repositories.Create
{
    public interface IPotRepository : IBaseRepository<Pot>
    {
        Pot GetByID(int id);
        IEnumerable<Pot> GetPotsByPlotID(int plotID);
        IEnumerable<Pot> GetListPots(int size = 10, int page = 1);
        bool CheckPlotID(int plotid);
        IEnumerable<PotServiceModel> GetAllPotByPlotID(int plotID);
        Pot GetPotWithUsernameAndPosition(string username, int position);
        IEnumerable<Pot> GetUserListPots(string username);
    }
    public class PotRepository : BaseRepository<Pot>, IPotRepository
    {
        public PotRepository(CapstoneDbContext context) : base(context) { }

        public Pot GetByID(int id) => Get().Where(p => p.Id == id && p.IsDelete == false).FirstOrDefault();
        public IEnumerable<Pot> GetPotsByPlotID(int plotID) => Get().Where(p => p.PlotId == plotID && p.IsDelete == false);
        public IEnumerable<PotServiceModel> GetAllPotByPlotID(int plotID) {
            var pots = Get().Where(p => p.PlotId == plotID && p.IsDelete == false);
            List<PotServiceModel> list = new List<PotServiceModel>();
            foreach (Pot p in pots)
            {
                PotServiceModel pot = new PotServiceModel
                {
                    ID = p.Id,
                    PlotID = p.PlotId,
                    Position = p.Position,
                    IsDelete = p.IsDelete,
                    Status = p.Status,
                    Coordinates = p.Coordinates
                };
                list.Add(pot);
            }
            IEnumerable<PotServiceModel> result = list;
            return result;
        }

        public IEnumerable<Pot> GetListPots(int size = 10, int page = 1)
        {
            return Get().Where(p => p.IsDelete == false).Skip((page - 1) * size).Take(size).ToList();
        }
        public bool CheckPlotID(int plotid)
        {
            var check = Get().Where(p => p.PlotId == plotid).FirstOrDefault();
            if (check != null)
            {
                return false;
            }
            return true;
        }

        public Pot GetPotWithUsernameAndPosition(string username, int position)
        {
            return (Pot)Get().Where(p => (p.Plot.Username == username && p.Plot.IsDelete == false) && p.Position == position).FirstOrDefault();
                
        }
        public IEnumerable<Pot> GetUserListPots(string username)
        {
            return Get().Where(p => (p.Plot.Username == username && p.Plot.IsDelete == false) && p.IsDelete == false);
        }
    }
}
