﻿using DataService.DesignPatern.Repositories.Basic;
using DataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Repositories.Create
{
    public interface IDiseaseRepository : IBaseRepository<Disease>
    {
        Disease GetByLabelId(int labelId);
    }
    public class DiseaseRepository : BaseRepository<Disease>, IDiseaseRepository
    {
        public DiseaseRepository(CapstoneDbContext context) : base(context)
        {

        }

        public Disease GetByLabelId(int labelId)
        {
            return Get().Where(d => d.LabelId == labelId && d.IsDelete == false).FirstOrDefault();
        }
    }
}
