﻿using DataService.DesignPatern.Repositories.Basic;
using DataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Repositories.Create
{
    public interface IScheduleRepository : IBaseRepository<Schedule>
    {
        Schedule GetUserScheduleByID(string username, int id);
        IEnumerable<Schedule> GetUserSchedules(string username);
        IEnumerable<Schedule> GetSchedules();
        IEnumerable<Schedule> GetSchedules(int size = 10, int page = 1);
    }

    public class ScheduleRepository : BaseRepository<Schedule>, IScheduleRepository
    {
        public ScheduleRepository(CapstoneDbContext Context) : base(Context) { }

        public Schedule GetUserScheduleByID(string username, int id) => Get().Where(p => p.Username == username && p.Id == id).FirstOrDefault();
        public IEnumerable<Schedule> GetUserSchedules(string username) => Get().Where(p => p.Username == username && p.IsDelete == false);

        public IEnumerable<Schedule> GetSchedules(int size = 10, int page = 1)
        {
            return Get().Where(p => p.IsDelete == false).Skip((page - 1) * size).Take(size).ToList();
        }
        public IEnumerable<Schedule> GetSchedules()
        {
            return Get().Where(p => p.IsDelete == false);
        }
    }
}
