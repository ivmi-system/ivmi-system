﻿using DataService.DesignPatern.Repositories.Basic;
using DataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataService.DesignPatern.Repositories.Create
{
    public interface IRequestRepository : IBaseRepository<Request>
    {
        IEnumerable<Request> GetAllRequests(int filter = 0, int size = 10, int page = 1);
    }
    public  class RequestRepository : BaseRepository<Request>, IRequestRepository
    {
        public RequestRepository(CapstoneDbContext context) : base(context) { }

        public IEnumerable<Request> GetAllRequests(int filter = 0, int size = 10, int page = 1)
        {
            switch (filter)
            {
                case 1:
                    return Get().Where(p => p.Status == filter && p.IsDelete == false).Skip((page - 1) * size).Take(size).ToList();
                default:
                    return Get().Where(p => p.IsDelete == false).Skip((page - 1) * size).Take(size).ToList();
            }
        }
    }
}
