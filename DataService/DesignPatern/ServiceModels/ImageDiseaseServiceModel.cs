﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.ServiceModels
{
    public class ImageDiseaseServiceModel
    {
        public int id { get; set; }
        public int VegetableId { get; set; }
        public string Vegetable { get; set; }
        public string Path { get; set; }
        public string FeedbackContent { get; set; }
        public bool IsDelete { get; set; }
    }
}
