﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.ServiceModels
{
    public class PotServiceModel
    {
        public int ID { get; set; }
        public int? PlotID { get; set; }
        public int? Position { get; set; }
        public int? Status { get; set; }
        public bool? IsDelete { get; set; }
        public string Coordinates { get; set; }
    }
}
