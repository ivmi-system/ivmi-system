﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.ServiceModels
{
    public class NotificationServiceModel
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public int? PlotID { get; set; }
        public bool? IsSeen { get; set; }
        public string Description { get; set; }
        public DateTime? DateTime { get; set; }
        public bool? IsDelete { get; set; }
    }
}
