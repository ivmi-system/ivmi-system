﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.ServiceModels
{
    public class ContractServiceModel
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime Duration { get; set; }
        public double? Fee { get; set; }
        public int Status { get; set; }
        public bool IsDelete { get; set; }
    }
}
