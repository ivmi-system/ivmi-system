﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.ServiceModels
{
   public class LoginServiceModel
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public int Role { get; set; }
        public int? ContractStatus { get; set; }
    }
}
