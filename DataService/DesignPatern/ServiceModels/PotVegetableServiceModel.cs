﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.ServiceModels
{
    public class PotVegetableServiceModel
    {
        public int ID { get; set; }
        public int? PotID { get; set; }
        public int? VegetableId { get; set; }
        public DateTime? DatePlant { get; set; }
        public DateTime? DateHarvest { get; set; }
        public int? Status { get; set; }
        public string UrlImage { get; set; }
        public bool? IsDelete { get; set; }
    }
}
