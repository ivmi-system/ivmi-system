﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.ServiceModels
{
    public class PlotServiceModel
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public string Line { get; set; }
        public int? Size { get; set; }
        public bool? IsDelete { get; set; }

        [Newtonsoft.Json.JsonProperty(NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore)]
        public IEnumerable<PotServiceModel> Pots { get; set; }
    }
}
