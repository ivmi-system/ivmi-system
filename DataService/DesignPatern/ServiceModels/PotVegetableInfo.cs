﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.ServiceModels
{
    public class PotVegetableInfo
    {
        public int PotId { get; set; }
        public int Position { get; set; }
        public int VegetableId { get; set; }
        public int potVegetableId { get; set; }
        public int PlotId { get; set; }
        public string UrlImage { get; set; }
        public string Discription { get; set; }
        public string Name { get; set; }
        public string DateHarvest { get; set; }
        public List<int> ListDiseases { get; set; }
        public bool? HasWorm { get; set; }
        public int? Status { get; set; }
        public bool Resolve { get; set; }

    }
}
