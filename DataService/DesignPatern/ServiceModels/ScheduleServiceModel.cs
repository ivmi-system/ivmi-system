﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.ServiceModels
{
    public class ScheduleServiceModel
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public bool? IsDelete { get; set; }
        public TimeSpan? Time { get; set; }
    }
}
