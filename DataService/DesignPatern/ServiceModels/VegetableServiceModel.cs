﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.ServiceModels
{
   public class VegetableServiceModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int? LabelId { get; set; }
        public int? HarvestFrom { get; set; }
        public int? HarvestTo { get; set; }
        public string Description { get; set; }
        public string Thumbnail { get; set; }
        public bool? IsDelete { get; set; }
    }
}
