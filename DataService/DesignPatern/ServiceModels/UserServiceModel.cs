﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.ServiceModels
{
    public class UserServiceModel
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string DeviceToken { get; set; }
        public string Phone { get; set; }
        public int Role { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string CameraIp { get; set; }
        public bool? IsDelete { get; set; }
        public bool IsContracted { get; set; }
    }
}
