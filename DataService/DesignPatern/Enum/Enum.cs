﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataService.DesignPatern.Enum
{
    public static class StatusMessage
    {
        public const string SUCCESS = "SUCCESS";
        public const string FAILED = "FAILED";
    }
    public static class Role
    {
        public const int USER = 1;
        public const int ADMIN = 2;
    }

    public static class UserEnum
    {
        public const int BLOCK = 1;
        public const int UNBLOCK = 2;
    }

    public static class PotStatus
    {
        public const int PLANT = 1;
        public const int BLANK = 2;
    }

    public static class PotVegetableDiseaseStatus
    {
        public const int UN_RESOLVE = 0;
        public const int RESOLVED = 1;
        public const int DELAY = 2;
    }
    public static class PotVegetableStatus
    {
        public const int PLANTING = 1;
        public const int HARVESTED = 2;
        public const int PENDING = 3;
    }
    public static class RequestStatus
    {
        public const int PENDING = 1;
        public const int COMFIRM = 2;
        public const int DONE = 3;
        public const int REJECT = 4;
    }
    public static class ContractStatus
    {
        public const int NOT_YET = 0;
        public const int ONGOING = 1;
        public const int EXPRIED = 2;
        public const int COMING_SOON = 3;
        public const int PAUSED = 4;
    }
    public static class ScheduleEnum
    {
        public static TimeSpan DEFAULT_AM = TimeSpan.Parse("06:00");
        public static TimeSpan DEFAULT_PM = TimeSpan.Parse("18:00");
    }
}
