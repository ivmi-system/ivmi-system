﻿using DataService.DesignPatern.UnitOfWorks;
using DataService.WriterHelper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vegetable.Web.API.ImageHandle
{
    public interface IImageHandler
    {
        Task<IActionResult> UploadImage(IFormFile file);
        Task<IActionResult> UploadImageForEachPlots(IFormFile file, int plotID);
    }

    public class ImageHandler : BaseUnitOfWork<UnitOfWork>, IImageHandler 
    {
        private readonly IImageWriter _imageWriter;
        public ImageHandler(IImageWriter imageWriter, UnitOfWork uow) : base(uow)
        {
            _imageWriter = imageWriter;
        }


        public async Task<IActionResult> UploadImage(IFormFile file)
        {
            var result = await _imageWriter.UploadImage(file);
            return new ObjectResult("http://important.reso.vn/images/" + result);
        }

        public async Task<IActionResult> UploadImageForEachPlots(IFormFile file, int plotID)
        {
            var plot = _uow.PotVegetable.Get().Where(p => p.PotId == plotID && p.IsDelete == false).FirstOrDefault();
            var result = await _imageWriter.UploadImage(file);
            return new ObjectResult("http://important.reso.vn/images/" + result);
        }
    }
}
