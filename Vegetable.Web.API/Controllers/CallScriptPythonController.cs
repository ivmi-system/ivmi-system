﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataService.DesignPatern.Enum;
using DataService.DesignPatern.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Vegetable.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CallScriptPythonController : ControllerBase
    {
        CallScriptPythonService _service;
        public CallScriptPythonController(CallScriptPythonService service)
        {
            _service = service;
        }
        [HttpGet]
        public IActionResult CallScriptPython()
        {
            var result = _service.GetImageFromIP();
            if (!string.IsNullOrEmpty(result))
            {
                return new JsonResult(new
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get image from ip camera success"
                });
            }
            else
            {
                return new JsonResult(new
                {
                    status = StatusMessage.FAILED,
                    message = "Get image from ip camera failed"
                });
            }


        }
    }
}
