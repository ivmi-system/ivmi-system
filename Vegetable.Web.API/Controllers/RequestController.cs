﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataService.DesignPatern.Enum;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Vegetable.Web.API.Controllers
{
    [Route("api/requests")]
    [ApiController]
    public class RequestController : ControllerBase
    {
        private RequestService _requestService;
        public RequestController(RequestService requestService)
        {
            _requestService = requestService;
        }

        #region Get all requests
        // [Authorize(Roles = "2")]
        [HttpGet]
        [Route("")]
        public IActionResult GetAllRequests(int filter = 0, int size = 10, int page = 1)
        {
            var requests = _requestService.GetAllRequests(filter, size, page);

            if (requests != null && requests.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get success",
                    data = requests
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Get failed"
            }.GetResponse();
        }
        #endregion

        #region Create new
        // [Authorize(Roles = "1, 2")]
        [HttpPost]
        [Route("")]
        public IActionResult Create(CreateRequestRequestModel model)
        {
            var request = _requestService.Create(model);

            if (request != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Created",
                    data = request
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Create failed"
            }.GetResponse();
        }
        #endregion

        #region Update status
        // [Authorize(Roles = "1, 2")]
        [HttpPatch]
        [Route("{id}")]
        public IActionResult UpdateStatus(int id, int status)
        {
            var request = _requestService.UpdateStatus(id, status);

            if (request != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Updated",
                    data = request
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Update failed"
            }.GetResponse();
        }
        #endregion
    }
}
