﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataService.DesignPatern.Enum;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Vegetable.Web.API.Controllers
{
    [Route("api/contracts")]
    [ApiController]
    public class ContractController : ControllerBase
    {
        private ContractService _contractService;
        public ContractController(ContractService contractService)
        {
            _contractService = contractService;
        }

        #region Get contracts
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("")]
        public IActionResult GetContracts(int size = 10, int page = 1)
        {
            var contracts = _contractService.GetContracts(size, page);

            if (contracts != null && contracts.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get contracts success",
                    data = contracts
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any contract"
            }.GetResponse();
        }
        #endregion

        #region Get contract by id
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetContractByID(int id)
        {
            var contract = _contractService.GetContractByID(id);

            if (contract != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get contract success",
                    data = contract
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any contract"
            }.GetResponse();
        }
        #endregion

        #region Create new contract
        // [Authorize(Roles = "1, 2")]
        [HttpPost]
        [Route("")]
        public IActionResult CreateNewContract(CreateContractRequestModel model)
        {
            var contract = _contractService.CreateNewContract(model);

            if (contract != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Create new contract success",
                    data = contract
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot create new contract"
            }.GetResponse();
        }
        #endregion

        #region Update contract
        // [Authorize(Roles = "1, 2")]
        [HttpPut]
        [Route("{id}")]
        public IActionResult UpdateContract(int id, UpdateContractRequestModel model)
        {
            var contract = _contractService.UpdateContract(id, model);

            if (contract != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Update contract success",
                    data = contract
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot update contract"
            }.GetResponse();
        }
        #endregion

        #region Pause contract
        // [Authorize(Roles = "1, 2")]
        [HttpPatch]
        [Route("{id}/pause")]
        public IActionResult PauseContract(int id)
        {
            var contract = _contractService.PauseContract(id);

            if (contract != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Paused",
                    data = contract
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Pause failed"
            }.GetResponse();
        }
        #endregion

        #region Delete contract
        // [Authorize(Roles = "1, 2")]
        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteContract(int id, bool mode)
        {
            bool result = _contractService.DeleteContract(id, mode);

            if (result)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Delete contract success",
                    data = id,
                    code = 200
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot delete contract"
            }.GetResponse();
        }
        #endregion
    }
}
