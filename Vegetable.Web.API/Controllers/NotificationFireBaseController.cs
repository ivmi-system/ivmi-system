﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DataService.DesignPatern.Enum;
using DataService.DesignPatern.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Vegetable.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationFireBaseController : ControllerBase
    {
       private readonly NotificationFireBaseService _service;
        public NotificationFireBaseController(NotificationFireBaseService service)
        {
            _service = service;
        }
        [HttpGet]
        public IActionResult SendNotification(int status)
        {
            string username = User.FindFirst(ClaimTypes.Name)?.Value ?? null;
            bool result = _service.SendNotification(username, status);
            if (result == true)
            {
                return new JsonResult(new
                {
                    status = StatusMessage.SUCCESS,
                    message = "Send notification success"
                });
            }
            else
            {
                return new JsonResult(new
                {
                    status = StatusMessage.FAILED,
                    message = "Send notification fail",
                });
            }
        }
    }
}
