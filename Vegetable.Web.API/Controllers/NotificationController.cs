﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataService.DesignPatern.Enum;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Services;
using Microsoft.AspNetCore.Mvc;



namespace Vegetable.Web.API.Controllers
{
    // [Authorize]
    [Route("api/notifications")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly NotificationService _service;
        public NotificationController(NotificationService service)
        {
            _service = service;
        }

        #region Get list notification
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("")]
        public IActionResult GetListNotification(int size = 10, int page = 1)
        {
            var notifications = _service.GetListNotifications(size, page);

            if (notifications != null && notifications.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get notifications success",
                    data = notifications
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any notifications"
            }.GetResponse();
        }
        #endregion

        #region Create new notification
        // [Authorize(Roles = "1, 2")]
        [HttpPost]
        [Route("")]
        public IActionResult CreateNewNotification(CreateNotificationRequestModel model)
        {
            var notifications = _service.CreateNewNotification(model);

            if (notifications != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Create new notification success",
                    data = notifications
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot create new notification"
            }.GetResponse();
        }
        #endregion

        #region Update seen vegetable
        // [Authorize(Roles = "1, 2")]
        [HttpPatch]
        [Route("seen-notification/{id}")]
        public IActionResult UpdateSeenNotification(int id)
        {
            var notification = _service.UpdateSeenNotification(id);

            if (notification)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Update seen notification success"
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot update seen notification"
            }.GetResponse();
        }
        #endregion

        #region Delete Notification
        // [Authorize(Roles = "1, 2")]
        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteNotification(int id, bool mode)
        {
            bool result = _service.DeleteNotification(id, mode);

            if (result)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Delete notification success",
                    data = result
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot delete notification"
            }.GetResponse();
        }
        #endregion

    }
}
