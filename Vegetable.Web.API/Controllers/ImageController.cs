﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataService.DesignPatern.Enum;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.ServiceModels;
using DataService.DesignPatern.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Vegetable.Web.API.ImageHandle;

namespace Vegetable.Web.API.Controllers
{
    [Route("api/images")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private readonly IImageHandler _imageHandler;
        private readonly ImageDiseaseService _service;
        public ImageController(IImageHandler imageHandler, ImageDiseaseService service)
        {
            _imageHandler = imageHandler;
            _service = service;
        }

        #region Get all
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("")]
        public IActionResult GetAll(int size = 10, int page = 1)
        {
            var images = _service.GetAll(size, page);

            if (images != null && images.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get success",
                    data = images
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get anny image"
            }.GetResponse();
        }
        #endregion

        #region upload
        // [Authorize(Roles = "1, 2")]
        [HttpPost]
        [Route("upload")]
        public async Task<IActionResult> UploadImage(IFormFile file)
        {
            if (file == null)
            {
                return BadRequest("Input image");
            }
            var result = await _imageHandler.UploadImage(file);
            if (result == null)
            {
                return BadRequest("Can't update image");
            }
            return result;
        }
        #endregion

        #region
        // [Authorize(Roles = "1, 2")]
        [HttpPost]
        [Route("")]
        public IActionResult Create(CreateImageDiseaseRequestModel model)
        {
            ImageDiseaseServiceModel imageDisease = _service.Create(model);

            if (imageDisease != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Created",
                    data = imageDisease
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Create fail"
            }.GetResponse();
        }
        #endregion
    }
}
