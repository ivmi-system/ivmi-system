﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataService.DesignPatern.Enum;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Services;
using DataService.Models;
using Microsoft.AspNetCore.Mvc;



namespace Vegetable.Web.API.Controllers
{
    // [Authorize]
    [Route("api/pots")]
    [ApiController]
    public class PotController : ControllerBase
    {
        private readonly PotService _potservice;
        public PotController(PotService service)
        {
            _potservice = service;
        }

        #region Get list pot
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("")]
        public IActionResult GetListPot(int size = 10, int page = 1)
        {
            var notifications = _potservice.GetListPots(size, page);

            if (notifications != null && notifications.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get pots success",
                    data = notifications
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any pots"
            }.GetResponse();
        }
        #endregion
        
        #region Create new pot
        // [Authorize(Roles = "1, 2")]
        [HttpPost]
        [Route("")]
        public IActionResult CreateNewPot(CreatePotRequestModel model)
        {
            var pot = _potservice.CreateNewPot(model);

            if (pot != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Create new pot success",
                    data = pot
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot create new pot"
            }.GetResponse();
        }
        #endregion

        #region Update Pot
        // [Authorize(Roles = "1, 2")]
        [HttpPut]
        [Route("{id}")]
        public IActionResult UpdatePot(int id, UpdatePotRequestModel model)
        {
            var pot = _potservice.UpdatePot(id, model);

            if (pot != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Update pot success",
                    data = pot
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot update pot"
            }.GetResponse();
        }
        #endregion

        #region Delete pot
        // [Authorize(Roles = "1, 2")]
        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeletePot(int id, bool mode)
        {
            bool result = _potservice.DeletePot(id, mode);

            if (result)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Delete pot success",
                    data = result
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot delete pot"
            }.GetResponse();
        }
        #endregion

        #region Get Pot with username and pot.position
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("position")]
        public IActionResult GetPotWithUsernameAndPosition(string username, int position)
        {
            Pot pot = _potservice.GetPotWithUsernameAndPosition(username, position);

            if (pot != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get pot with position and username success",
                    data = pot
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get pot with position and username"
            }.GetResponse();
        }
        #endregion

        #region Get user list pots
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("get-all-pot-user")]
        public IActionResult GetUserListPots(string username)
        {
            IEnumerable<Pot> pots = _potservice.GetUserListPots(username);

            if (pots != null && pots.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get user's pot success",
                    data = pots
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any user's pot"
            }.GetResponse();
        }
        #endregion

        #region Update Coordinates
        //// [Authorize(Roles = "1, 2")]
        //[HttpPatch]
        //[Route("")]
        //public IActionResult UpdateCoordinates(UserCoordinatesRequestModel model)
        //{
        //    bool result = _potservice.UpdateCoordinates(model.id, model.coordinates);

        //    if (result)
        //    {
        //        return new ApiResponse
        //        {
        //            status = StatusMessage.SUCCESS,
        //            message = "Pot's coordinates updated",
        //            data = result
        //        }.GetResponse();
        //    }

        //    return new ApiResponse
        //    {
        //        status = StatusMessage.FAILED,
        //        message = "Cannot update pot's coordinates"
        //    }.GetResponse();
        //}
        //
        [HttpPatch]
        [Route("{potId}")]
        public IActionResult UpdateCoordinate(int potId)
        {
            var result = _potservice.UpdateCoordinate(potId);
            if (result == true)
            {
                return new JsonResult(new
                {
                    status = StatusMessage.SUCCESS,
                    message = "Update coordinate success"
                });
            }
            else
            {
                return new JsonResult(new
                {
                    status = StatusMessage.FAILED,
                    message = "Update coordinate failed",
                    data = result
                });
            }
        }
        #endregion

        #region Harvest
        // [Authorize(Roles = "1, 2")]
        [HttpPatch]
        [Route("{id}/harvest")]
        public IActionResult Harvest(int id)
        {
            bool result = _potservice.Harvest(id);
            if (result)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Harvested",
                    data = result
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot harvest for this pot"
            }.GetResponse();
        }
        #endregion
    }

}
