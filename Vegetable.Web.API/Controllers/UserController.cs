﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using DataService.DesignPatern.Enum;
using DataService.DesignPatern.Handler;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.ServiceModels;
using DataService.DesignPatern.Services;
using DataService.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Vegetable.Web.API.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private UserService _userService;
        public UserController(UserService userService)
        {
            _userService = userService;
        }

        #region Get All User
        // [Authorize(Roles = "2")]
        [HttpGet]
        [Route("")]
        public IActionResult GetAllUser()
        {
            string currentUsername = User.FindFirst(ClaimTypes.Name)?.Value ?? null;
            var users = _userService.GetAllUser(currentUsername);

            if (users != null && users.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get lisr users success",
                    data = users
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any user"
            }.GetResponse();
        }
        #endregion

        #region Get All User with size and page
        // [Authorize(Roles = "2")]
        [HttpGet]
        [Route("get-all-users")]
        public IActionResult GetAllUser(int page, int size)
        {
            string currentUsername = User.FindFirst(ClaimTypes.Name)?.Value ?? null;
            var result = _userService.GetAllUser(currentUsername, size, page);
            if (result == null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.FAILED,
                    message = "Data empty"
                }.GetResponse();
            }
            else
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get user",
                    data = result

                }.GetResponse();
            }
        }
        #endregion

        #region Check Login
        [HttpPost]
        [Route("login")]
        public IActionResult Login(LoginRequestModel model)
        {
            var user = _userService.CheckLogin(model);
            if (user == null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.FAILED,
                    message = "Username or Password is not correct"
                }.GetResponse();
            }
            else
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Login success",
                    data = user

                }.GetResponse();
            }
        }
        #endregion

        #region Create User
        [Authorize(Roles ="2")]
        [HttpPost]
        [Route("create")]
        public IActionResult Create(CreateUserRequestModel model)
        {
            var user = _userService.CreateAccount(model);
            if (user == null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.FAILED,
                    message = "Create failed"
                }.GetResponse();
            }
            else
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Create success",
                    data = user
                }.GetResponse();
            }
        }
        #endregion

        #region Update Device Token
        [Authorize]
        [HttpPatch]
        [Route("update-token")]
        public IActionResult Update(string deviceToken)
        {
            string username = User.FindFirst(ClaimTypes.Name)?.Value ?? null;
            bool result = _userService.UpdateDeviceToken(username,deviceToken);
                if (result == true)
                {
                    return new ApiResponse
                    {
                        status = StatusMessage.SUCCESS,
                        message = "update token succes"
                    }.GetResponse();
                }
                else
                {
                    return new ApiResponse
                    {
                        status = StatusMessage.FAILED,
                        message = "update token fail",
                    }.GetResponse();
                }
            }
        #endregion

        #region Block/Unblock User
        [Authorize(Roles = "2")]
        [HttpPatch]
        [Route("{username}/block-unblock/{mode}")]
        public IActionResult BlockAndUnBlockUser(string username, int mode)
        {
            string currentUser = User.FindFirst(ClaimTypes.Name)?.Value ?? null;

            var message = mode == 1 ? "Block" : "Unblock";


            if (currentUser == username)
            {
                return new ApiResponse
                {
                    status = StatusMessage.FAILED,
                    message = message + " failed"
                }.GetResponse();
            }
            var user = _userService.BlockAndUnBlockAccount(username, mode);
            if(user == true)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = message + " success"
                }.GetResponse();
            }

            else
            {
                return new JsonResult(new
                {
                    status = StatusMessage.FAILED,
                    message = message + " failed"
                });
            }
        }
        #endregion

        #region Get User By Username
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{username}")]
        public IActionResult getUserByUsername(string username)
        {
            var user = _userService.GetUserByUsername(username);

            if (user != null)
            {
                return new ApiResponse
                { 
                    status = StatusMessage.SUCCESS,
                    message = "Get user success",
                    data = user
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "User is not exist or was block"
            }.GetResponse();
        }
        #endregion

        #region Update user
        [Authorize(Roles = "2, 1")]
        [HttpPut]
        [Route("{username}")]
        public IActionResult UpdateUser(string username, UpdateUserRequestModel model)
        {
            User user = _userService.UpdateUser(username, model);

            if (user != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Update user success",
                    data = user
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot update this user"
            }.GetResponse();
        }
        #endregion

        #region Get user's schedule
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{username}/schedules")]
        public IActionResult GetUserSchedule(string username)
        {
            var schedules = _userService.GetUserSchedules(username);

            if (schedules != null && schedules.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get user's schedules success",
                    data = schedules
                }.GetResponse();
            }

            return new ApiResponse
            { 
                status = StatusMessage.FAILED,
                message = "Cannot get any user's schedule"
            }.GetResponse();
        }
        #endregion

        #region Get user's schedule by schedule id
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{username}/schedules/{id}")]
        public IActionResult GetUserSchedule(string username, int id)
        {
            Schedule schedule = _userService.GetUserScheduleByID(username, id);

            if (schedule != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get user's schedule success",
                    data = schedule
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any user's schedule"
            }.GetResponse();
        }
        #endregion

        #region Get user's plots
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{username}/plots")]
        public IActionResult GetUserPlots(string username, bool show_pots = false)
        {
            IEnumerable<PlotServiceModel> plots = _userService.GetUserPlots(username, show_pots);
            if (plots != null && plots.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get plots success",
                    data = plots
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any plot"
            }.GetResponse();
        }
        #endregion

        #region Get user's plot by plot id
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{username}/plots/{id}")]
        public IActionResult GetUserPlotByID(string username, int id)
        {
            Plot plot = _userService.GetUserPlotByID(username, id);

            if (plot != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get plot success",
                    data = plot
                }.GetResponse();
            }

            ApiResponse response = new ApiResponse
            {
                code = 200,
                status = StatusMessage.FAILED,
                message = "Cannot get any plot"
            };

            return response.GetResponse();
        }
        #endregion

        #region Get user's notifications
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{username}/notifications")]
        public IActionResult GetUserNotifications(string username, int size = 10, int page = 1)
        {
            var notifications = _userService.GetUserNotifications(username, size, page);

            if (notifications != null && notifications.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get notifications success",
                    code = 200,
                    data = notifications
                }.GetResponse();
            }

            return new ApiResponse 
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any notifications",
                code = 200
            }.GetResponse();
        }
        #endregion

        #region Get user's notifications
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{username}/notifications/{seen}")]
        public IActionResult GetUserNotifications(string username, bool seen, int size = 10, int page = 1)
        {
            var notifications = _userService.GetUserNotifications(username, seen, size, page);

            if (notifications != null && notifications.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get notifications success",
                    code = 200,
                    data = notifications
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any notifications",
                code = 200
            }.GetResponse();
        }
        #endregion

        #region Mark as read for all notification of user
        // [Authorize(Roles = "1, 2")]
        [HttpPut]
        [Route("{username}/notifications/mark-as-read")]
        public IActionResult markAsReadNotification(string username)
        {
            bool result = _userService.MarkAsReadNotifications(username);

            if (result)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Mark as read notifications success",
                    code = 200
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot update notifications",
                code = 500
            }.GetResponse();
        }
        #endregion

        #region Get user's contracts
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{username}/contracts")]
        public IActionResult GetUserContracts(string username, int size = 10, int page = 1, bool lastest = false)
        {
            var contracts = _userService.GetUserContracts(username, size, page, lastest);

            if (contracts != null && contracts.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get contract success",
                    data = contracts
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any contract"
            }.GetResponse();
        }
        #endregion

        #region Get user lastest contract by duration
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{username}/contracts/lastest")]
        public IActionResult GetUserContractLastest(string username, string type)
        {
            Contract result = null;
            switch (type)
            {
                default:
                    result = _userService.GetLastestDurationUser(username);
                    break;
            }

            if (result != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get lastest contract success",
                    data = result
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any contract"
            }.GetResponse();
        }
        #endregion

        #region Start running schedule job (detect) for user
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{username}/schedules/start")]
        public IActionResult StartUserSchedule(string username)
        {
            bool result = _userService.StartUserSchedule(username);

            if (result)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Start succes"
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot start"
            }.GetResponse();
        }
        #endregion

        #region Start running schedule job (detect) for user
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{username}/schedules/stop")]
        public IActionResult StopUserSchedule(string username)
        {
            bool result = _userService.StopUserSchedule(username);

            if (result)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Stop succes schedules"
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot stop schedules"
            }.GetResponse();
        }
        #endregion

        #region Check user can create new contract or not
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{username}/can-new-contract")]
        public IActionResult IsAvailableNewContract(string username)
        {
            bool result = _userService.IsAvailableNewContract(username);

            return new ApiResponse
            {
                status = result? StatusMessage.SUCCESS : StatusMessage.FAILED,
                data = result,
                message = result.ToString()
            }.GetResponse();
        }
        #endregion
    }

}


