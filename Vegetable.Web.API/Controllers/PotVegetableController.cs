﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataService.DesignPatern.Enum;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Services;
using Microsoft.AspNetCore.Mvc;



namespace Vegetable.Web.API.Controllers
{
    // [Authorize]
    [Route("api/potVegetable")]
    [ApiController]
    public class PotVegetableController : ControllerBase
    {
        private readonly PotVegetableService _potvegetableservice;
        public PotVegetableController(PotVegetableService service)
        {
            _potvegetableservice = service;
        }

        #region Get list potVegetable
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("")]
        public IActionResult GetListPotVegetable(int size = 10, int page = 1)
        {
            var potvegetable = _potvegetableservice.GetListPotVegetable(size, page);

            if (potvegetable != null && potvegetable.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get potVegetable success",
                    data = potvegetable
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any potVegetable"
            }.GetResponse();
        }
        #endregion

        #region Create new potVegetable
        // [Authorize(Roles = "1, 2")]
        [HttpPost]
        [Route("")]
        public IActionResult CreateNewPotVegetable(CreatePotVegetableRequestModel model)
        {
            var potvegetable = _potvegetableservice.CreateNewPotVegetable(model);

            if (potvegetable != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Create new potVegetable success",
                    data = potvegetable
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot create new potVegetable"
            }.GetResponse();
        }
        #endregion
        
        #region Update PotVegetable
        // [Authorize(Roles = "1, 2")]
        [HttpPut]
        [Route("{id}")]
        public IActionResult UpdatePotVegetable(UpdatePotVegetableRequestModel model)
        {
            var potvegetable = _potvegetableservice.UpdatePotVegetable(model);

            if (potvegetable != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Update potVegetable success",
                    data = potvegetable
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot update potVegetable"
            }.GetResponse();
        }
        #endregion

        #region Delete pot
        // [Authorize(Roles = "1, 2")]
        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeletePotVegetable(int id, bool mode)
        {
            bool result = _potvegetableservice.DeletePotVegetable(id, mode);

            if (result)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Delete potVegetable success",
                    data = result
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot delete potVegetable"
            }.GetResponse();
        }
        #endregion

        //#region Harvest: Update status
        //// [Authorize(Roles = "1, 2")]
        //[HttpPost]
        //[Route("{id}/harvest")]
        //public IActionResult Harvest(int id)
        //{
        //    bool result = _potvegetableservice.Harvest(id);

        //    if (result)
        //    {
        //        return new ApiResponse
        //        {
        //            status = StatusMessage.SUCCESS,
        //            message = "Harvested",
        //            data = result
        //        }.GetResponse();
        //    }

        //    return new ApiResponse
        //    {
        //        status = StatusMessage.FAILED,
        //        message = "Cannot harvest"
        //    }.GetResponse();
        //}
        //#endregion
    }
}
