﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataService.DesignPatern.Enum;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Vegetable.Web.API.Controllers
{
    // [Authorize]
    [Route("api/diseases")]
    [ApiController]
    public class DiseaseController : ControllerBase
    {

        private DiseaseService _diseaseService;
        public DiseaseController(DiseaseService diseaseService)
        {
            _diseaseService = diseaseService;
        }

        #region Create new disease
        // [Authorize(Roles = "1, 2")]
        [HttpPost]
        [Route("")]
        public IActionResult CreateNewDisease(CreateDiseaseRequestModel model)
        {
            var disease = _diseaseService.CreateNewDisease(model);

            if (disease != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Create new disease success",
                    data = disease
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot create disease"
            }.GetResponse();
        }
        #endregion

        #region Get disease by ID
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetDiseaseByID(int id)
        {
            var disease = _diseaseService.GetDiseaseID(id);

            if (disease != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get disease success",
                    data = disease
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any disease"
            }.GetResponse();
        }

        #endregion

        #region Update disease
        // [Authorize(Roles = "1, 2")]
        [HttpPut]
        [Route("{id}")]
        public IActionResult UpdateDisease(int id, UpdateDiseaseRequestModel model)
        {
            var disease = _diseaseService.UpdateDisease(id, model);

            if (disease != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Update disease success",
                    data = disease
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot update disease"
            }.GetResponse();
        }
        #endregion

        #region Delete disease
        // [Authorize(Roles = "1, 2")]
        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteDisease(int id, bool mode)
        {
            bool result = _diseaseService.DeleteDisease(id, mode);

            if (result)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Delete disease success",
                    data = result
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot delete disease"
            }.GetResponse();
        }
        #endregion
    }
}
