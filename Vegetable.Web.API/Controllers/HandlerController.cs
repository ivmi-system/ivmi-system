﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataService.DesignPatern.Enum;
using DataService.DesignPatern.Handler;
using DataService.DesignPatern.Services;
using FluentScheduler;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Vegetable.Web.API.Controllers
{
    [Route("api/handlers")]
    [ApiController]
    public class HandlerController : ControllerBase
    {
        private readonly Registry _registry;
        private HandlerService _service;
        public HandlerController(HandlerService service)
        {
            this._registry = new Registry();
            _service = service;
        }

        #region Get list of schedules is running
        [HttpGet]
        [Route("")]
        public IActionResult GetAllSchedules()
        {
            var schedules = JobManager.AllSchedules.ToList();

            if (schedules != null && schedules.Any())
            {
                return new ApiResponse
                {
                    code = 200,
                    status = StatusMessage.SUCCESS,
                    message = "Get schedules success",
                    data = schedules
                }.GetResponse();
            }

            return new ApiResponse
            {
                code = 500,
                status = StatusMessage.FAILED,
                message = "No schedules are currently running."
            }.GetResponse();
        }
        #endregion

        #region Start all schedules
        [HttpGet]
        [Route("start")]
        public IActionResult StartAllSchedules()
        {
            _service.StartALlSchedules();

            return new ApiResponse
            {
                code = 200,
                status = StatusMessage.SUCCESS,
                message = "Schedules are now running."
            }.GetResponse();
        }
        #endregion

        #region Stop schedule with specific name
        [HttpGet]
        [Route("stop/{name}")]
        public IActionResult StopWithName(string name)
        {
            //var job = new HandlerJob
            //{
            //    JobName = name
            //};
            //job.StopJob();

            //if (!job.AnyJob())
            //{
            //    return new ApiResponse
            //    {
            //        code = 200,
            //        status = StatusMessage.SUCCESS,
            //        message = "Stop suceess with name " + name
            //    }.GetResponse();
            //}

            return new ApiResponse
            {
                code = 500,
                status = StatusMessage.FAILED,
                message = "Stop Failed with name " + name
            }.GetResponse();
        }
        #endregion
    }
}
