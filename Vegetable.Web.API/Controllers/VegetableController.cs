﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DataService.DesignPatern.Enum;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Vegetable.Web.API.Controllers
{
    // [Authorize]
    [Route("api/vegetables")]
    [ApiController]
    public class VegetableController : ControllerBase
    {
        private readonly VegetableService _service;
        public VegetableController(VegetableService service)
        {
            _service = service;
        }

        #region Get list vegetable
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("")]
        public IActionResult GetListVegetables(int size = 10, int page = 1)
        {
            var vegetables = _service.GetListVegetables(size, page);

            if (vegetables != null && vegetables.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get vegetables success",
                    data = vegetables
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any vegetable"
            }.GetResponse();
        }
        #endregion

        #region Create new vegetable
        // [Authorize(Roles = "1, 2")]
        [HttpPost]
        [Route("")]
        public IActionResult CreateNewVegetable(CreateVegetableRequestModel model)
        {
            var vegetable = _service.CreateNewVegetable(model);

            if (vegetable != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Create new vegetable success",
                    data = vegetable
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot create new vegetable"
            }.GetResponse();
        }
        #endregion

        #region Get vegetable
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetVegetableByID(int id)
        {
            var vegetable = _service.GetVegetableByID(id);

            if (vegetable != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get vegetable success",
                    data = vegetable
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any vegetable"
            }.GetResponse();
        }
        #endregion

        #region Update vegetable
        // [Authorize(Roles = "1, 2")]
        [HttpPut]
        [Route("{id}")]
        public IActionResult UpdateVegetable(int id, UpdateVegetableRequestModel model)
        {
            var vegetable = _service.UpdateVegetable(id, model);

            if (vegetable != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Update vegetable success",
                    data = vegetable
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot update vegetable"
            }.GetResponse();
        }
        #endregion

        #region Delete vegetable
        // [Authorize(Roles = "1, 2")]
        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteVegetable(int id, bool mode)
        {
            bool result = _service.DeleteVegetable(id, mode);

            if (result)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Delete vegetable success",
                    data = result
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot delete vegetable"
            }.GetResponse();
        }
        #endregion

        #region Detect and send notification to end user
        [HttpGet]
        [Route("detect-and-send-notification")]
        public IActionResult DetectAndSendNoti()
        {
            //string username = User.FindFirst(ClaimTypes.Name)?.Value ?? null;
            var result = _service.DetectAndSendNoti("sonph");
            if (result == true)
            {
                return new JsonResult(new
                {
                    status = StatusMessage.SUCCESS,
                    message = "Detect and send notification success"
                });
            }
            else
            {
                return new JsonResult(new
                {
                    status = StatusMessage.FAILED,
                    message = "Detect and send notification fail",
                });
            }
        }
        #endregion

        #region get-info-vegetable
        [HttpGet]
        [Route("get-info-vegetable")]
        public IActionResult GetAll(string username)
        {
            var result = _service.GetAllPotVegetableInfo(username);
            if (result != null)
            {
                return new JsonResult(new
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get infor success",
                    //statusVegetable ="Bị sâu bệnh",
                    data = result
                });
            }
            else
            {
                return new JsonResult(new
                {
                    status = StatusMessage.FAILED,
                    message = "Data empty",
                    data = result
                });
            }

        }
        #endregion
        [HttpPatch]
        [Route("{potVegetableId}")]
        public IActionResult ResolveDisease(int potVegetableId)
        {
            var result = _service.ResolveDisease(potVegetableId);
            if (result == true)
            {
                return new JsonResult(new
                {
                    status = StatusMessage.SUCCESS,
                    message = "Resolve success",
                    data = result
                });
            }
            else
            {
                return new JsonResult(new
                {
                    status = StatusMessage.FAILED,
                    message = "Resolve failed",
                    data = result
                });
            }
        }

        [HttpPatch]
        [Route("confirm-young-vegetable/{potId}")]
        public IActionResult ConfirmYoungVegetable(int potId,int day)
        {
            var result = _service.ConfirmYoungVegetable(potId,day);
            if(result == true)
            {
                return new JsonResult(new
                {
                    status = StatusMessage.SUCCESS,
                    message = "Confirm success"
                });
            }
            else
            {
                return new JsonResult(new
                {
                    status = StatusMessage.FAILED,
                    message = "Confirm failed"
                });
            }
        }

    }
}

