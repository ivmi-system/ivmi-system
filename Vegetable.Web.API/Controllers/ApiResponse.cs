﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vegetable.Web.API.Controllers
{
    public class ApiResponse
    {
        public string status { private get; set; }
        public int code { private get; set; }
        public string message { private get; set; }
        public Object data { private get; set; }

        public ApiResponse() { }

        private string GetCurrentTime()
        {
            return DateTime.Now.ToString();
        }

        public JsonResult GetResponse() => new JsonResult(new
        {
            this.status,
            this.code,
            this.message,
            timestamp = this.GetCurrentTime(),
            this.data
        });
    }
}
