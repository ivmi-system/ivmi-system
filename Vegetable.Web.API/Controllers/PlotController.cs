﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataService.DesignPatern.Enum;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Services;
using DataService.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Vegetable.Web.API.Controllers
{
    [Route("api/plots")]
    [ApiController]
    public class PlotController : ControllerBase
    {
        private PlotService _plotService;
        public PlotController(PlotService plotService)
        {
            _plotService = plotService;
        }

        #region Create new plot
        // [Authorize(Roles = "1, 2")]
        [HttpPost]
        [Route("")]
        public IActionResult CreateNewPlot(CreatePlotRequestModel model)
        {
            var plot = _plotService.CreateNewPlot(model);

            if (plot != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Create new plot success",
                    data = plot
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot create plot"
            }.GetResponse();
        }
        #endregion

        #region Get plot by ID
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetByID(int id)
        {
            var plot = _plotService.GetPlotByID(id);

            if (plot != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get plot success",
                    data = plot
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any plot"
            }.GetResponse();
        }

        #endregion

        #region Update plot
        // [Authorize(Roles = "1, 2")]
        [HttpPut]
        [Route("{id}")]
        public IActionResult UpdatePlot(int id, UpdatePlotRequesrModel model)
        {
            var plot = _plotService.UpdatePlot(id, model);

            if (plot != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Update plot success",
                    data = plot
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot update plot"
            }.GetResponse();
        }
        #endregion

        #region Delete plot
        // [Authorize(Roles = "1, 2")]
        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeletePlot(int id, bool mode)
        {
            bool result = _plotService.DeletePlot(id, mode);

            if (result)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Delete plot success",
                    data = result
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot delete plot"
            }.GetResponse();
        }
        #endregion

        #region Get Plot's pot
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{id}/pot")]
        public IActionResult GetPlotsPot(int id)
        {
            IEnumerable<Pot> schedules = _plotService.GetPlotsPot(id);

            if (schedules != null && schedules.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get Plots pot success",
                    data = schedules
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any pot"
            }.GetResponse();
        }
        #endregion

        #region Get plot's notifications
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{id}/notifications")]
        public IActionResult GetPlotNotifications(int id, int size = 10, int page = 1)
        {
            var notifications = _plotService.GetPlotNotifications(id, size, page);

            if (notifications != null && notifications.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get notifications success",
                    code = 200,
                    data = notifications
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any notifications",
                code = 200
            }.GetResponse();
        }
        #endregion
        
        #region Get pots by plot ID

        [HttpGet]
        [Route("{id}/pots")]
        public IActionResult GetPotsByPlotID(int id)
        {
            var pots = _plotService.GetPotsByPlotID(id);
            if (pots != null && pots.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get successful",
                    data = pots
                }.GetResponse();
            }
            return new ApiResponse
            {
                message = "Cannot get pots!",
                status = StatusMessage.FAILED
            }.GetResponse();
        }
        #endregion
       
    }
}
