﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataService.DesignPatern.Enum;
using DataService.DesignPatern.RequestModels;
using DataService.DesignPatern.Services;
using Microsoft.AspNetCore.Mvc;

namespace Vegetable.Web.API.Controllers
{
    [Route("api/schedules")]
    [ApiController]
    public class ScheduleController : ControllerBase
    {
        private ScheduleService _scheduleService;
        public ScheduleController(ScheduleService scheduleService)
        {
            _scheduleService = scheduleService;
        }

        #region Get list schedules
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("")]
        public IActionResult GetSchedules()
        {
            var schedules = _scheduleService.GetSchedules();

            if (schedules != null && schedules.Any())
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get list schedules scucess",
                    data = schedules
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any schedule"
            }.GetResponse();
        }
        #endregion

        #region Create new schedule
        // [Authorize(Roles = "1, 2")]
        [HttpPost]
        [Route("")]
        public IActionResult CreateNewSchedule(CreateScheduleRequestModel model)
        {
            var schedule = _scheduleService.CreateNewSchedule(model);

            if (schedule != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Create new schedule success",
                    data = schedule
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot create new schedule"
            }.GetResponse();
        }
        #endregion

        #region Get schedule by ID
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{id}")]
        public IActionResult GetScheduleByID(int id)
        {
            var schedule = _scheduleService.GetScheduleByID(id);

            if (schedule != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Get schedule success",
                    data = schedule
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot get any schedule"
            }.GetResponse();
        }
        #endregion

        #region Update chedule
        // [Authorize(Roles = "1, 2")]
        [HttpPut]
        [Route("{id}")]
        public IActionResult UpdateSchedule(int id, UpdateScheduleRequestModel model)
        {
            var schedule = _scheduleService.UpdateSchedule(id, model);

            if (schedule != null)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Update schedule success",
                    data = schedule
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot update schedule"
            }.GetResponse();
        }
        #endregion

        #region Delete schedule
        // [Authorize(Roles = "1, 2")]
        [HttpDelete]
        [Route("{id}")]
        public IActionResult DeleteSchedule(int id, bool mode)
        {
            bool result = _scheduleService.DeleteSchedule(id, mode);

            if (result)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Delete schedule success",
                    data = result
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot delete schedule"
            }.GetResponse();
        }
        #endregion

        #region Start schedule by ID
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{id}/start")]
        public IActionResult StartScheduleByID(int id)
        {
            bool result = _scheduleService.StartOrStopSchedule(id, true);

            if (result)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Start schedule"
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot start schedule"
            }.GetResponse();
        }
        #endregion

        #region Stop schedule by ID
        // [Authorize(Roles = "1, 2")]
        [HttpGet]
        [Route("{id}/stop")]
        public IActionResult StopScheduleByID(int id)
        {
            bool result = _scheduleService.StartOrStopSchedule(id, false);

            if (result)
            {
                return new ApiResponse
                {
                    status = StatusMessage.SUCCESS,
                    message = "Stop schedule"
                }.GetResponse();
            }

            return new ApiResponse
            {
                status = StatusMessage.FAILED,
                message = "Cannot stop schedule"
            }.GetResponse();
        }
        #endregion

    }
}
